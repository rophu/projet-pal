let groups;

class User{
  constructor(pseudo, email, lieu, description){
    this.pseudo = pseudo;
    this.email = email;
    this.lieu = lieu;
    this.description = description;
    this.privileges = "aucun";
  }
}

$(document).ready(function (){

//Click sur listing de group
$('#ListGroup').click(function (){
  $('#container').empty();
  $.ajax({
            url: "http://localhost:8000/PAL/api/v1.0/group",
            type: "GET",
            dataType : "json",
            success: function(req, status, err) {
                console.log(JSON.stringify(req));
                console.log(req.group.length);
                for (let i=0;i<req.group.length;i++){
                  console.log(i, req.group[i].id);
                  $('#container').append($('<p><u>').text("Titre de groupe : "+req.group[i].titre));
                  $('#container').append($('<p>').text("Description : "+req.group[i].desc));
                }
              },
            error: function(req, status, err) {
                        $().html("<b>Impossible de récupérer les taches à réaliser !</b>");

                        }
        });
      });

// Click sur listing user
$('#ListUser').click(function (){
  $('#container').empty();
  $.ajax({
    url: "http://localhost:8000/PAL/api/v1.0/user",
    type: "GET",
    dataType: "json",
    success: function (rep, status, err){
      console.log(JSON.stringify(rep));
      for (let i=0;i<rep.user.length;++i){
        $('#container').append($('<p><u>').text("Pseudo User :"+rep.user[i].pseudo));
        $('#container').append($('<p>').text("Email :"+rep.user[i].email));
        $('#container').append($('<p>').text("Description :"+rep.user[i].description));
      }
    },
    error : function (rep, status, err){
      $().html("Erreur, impossible de récupérer les utilisateurs ! 0.o");
    }
  });

});

$('#addUser').click(function(){
  $('#container').empty();
  $('#container').append($('<p>').text("pseudo :").append($('<input id="pseudo">')));
  $('#container').append($('<p>').text("email :").append($('<input id="email">')));
  $('#container').append($('<p>').text("lieu :").append($('<input id="lieu">')));
  $('#container').append($('<p>').text("description :").append($('<input id="description">')));
  $('#container').append($('<p id="valid">').text("Valider"));
  let donne = {
          "pseudo": "Alicia",
          "email": "gussie.auer@yahoo.com",
          "lieu": "Fleury-les-Aubraies",
          "description": "Quos eos beatae sunt consectetur id in illum.",
          "privileges": "aucun"
        }
  $('#valid').click(function(){

    const datas = new User(
      $('#pseudo').val(),
      $('#email').val(),
      $('#lieu').val(),
      $('#description').val()
    );
    console.log(datas);
    console.log(JSON.stringify(datas));
    $.ajax({
      url: "http://localhost:8000/PAL/api/v1.0/user",
      type: "POST",
      contentType: 'application/json',
      dataType: "json",
      data: JSON.stringify(datas),
      success: function (rep, status, err){
        console.log('REP :',rep);
        console.log('STAT :',status);
        console.log('ERR :',err);
      },
      error: function (rep, status, err){
        $().html("Erreur, impossible de récupérer les utilisateurs ! 0.o");
      }
    });
  });
});


//Gestion de l'utilisateurs
$('#UserSpace').click(function(){
  //Connexion de l'utilisateur
  $('#container').empty();
  $('#container').append($('<p>').text("Votre Pseudo : "))
  .append($('<input id="pseudo">'))
  .append($('<p id="valid">').text("Valider"));

$('#valid').click(function (){

  let pseudo = $('#pseudo').val();
  $.ajax({
    url: "http://localhost:8000/PAL/api/v1.0/user/"+pseudo,
    type: "GET",
    contentType: 'application/json',
    dataType: 'json',
    success: function (rep, status, err){
      console.log(JSON.stringify(rep));
        $('#infos').empty();
        $('#container').append($('<div id="infos">'))
        $('#infos').append($('<p><u>').text("Pseudo User :"+rep.pseudo));
        $('#infos').append($('<p>').text("Email :"+rep.email));
        $('#infos').append($('<p>').text("Description :"+rep.description));
      },
    error : function (rep, status, err){
      $().html("Erreur, impossible de récupérer les utilisateurs ! 0.o");
    }}
    );
  });
});

});
