// Fonction pour gérer l'affichage et la gestion des Membres
// de l'association

class User{
  constructor(pseudo, email, lieu, description){
    this.pseudo = pseudo;
    this.email = email;
    this.lieu = lieu;
    this.description = description;
    this.privileges = "aucun";
  }
}


function ListingUser(){

$.ajax({
    url: "http://localhost:8000/PAL/api/v1.0/user",
    type: "GET",
    dataType : "json",
    success: function(rep, status, err) {
        console.log(JSON.stringify(rep));
        DomBuilderUser(rep.user);
      },
    error: function(rep, status, err) {
      $('#container').html("<b>Impossible de récupérer les informations utilisateurs !</b>");

      }
    });

}

function DomBuilderUser(user){
  //Fonction qui construit la liste des user dans le DON
  // via les info ramassées en JSON
  $('#container').empty();
  $('#container').append($('<div id="actions">'));
  $('#container').append($('<div id="liste">'));
  $('#container').append($('<div id="detail">'));

  $('#actions').append($('<p id="ajout-user" onclick="ajouterUser()">').text('Ajouter Un User '));
  $('#actions').append($('<p id="suppr-user">').text('|  Supprimer Un User '));
//  $('#suppr-user').attr( "onclick","supprimerUser(user)");
  $('#actions').append($('<p id="modif-user" onclick="modifierUser()">').text('|  Modifier Un User '));

  $('#liste').append($('<table id="table-user">'));
  $('#table-user').append($('<tr id="head-row">'));
  $('#head-row').append($('<td>').text("ID"));
  $('#head-row').append($('<td>').text("Pseudo"));
  $('#head-row').append($('<td>').text("Email"));
  $('#head-row').append($('<td>').text("Détails"));

  console.log(user.length);
  for (let i=0;i<user.length;i++){
    $('#table-user').append($('<tr id="user_'+user[i].id+'"onclick=DetailsUser('+user[i].id+')>'));

    $('#user_'+user[i].id).append($('<td>').text(user[i].pseudo));
    $('#user_'+user[i].id).append($('<td>').text(user[i].email));
    $('#user_'+user[i].id).append($('<td>').text(user[i].lieu));
    $('#user_'+user[i].id).append($('<td>').text('Voir les détails'));

  }

  // Est-ce bien la seule maniêre de faire ? problème intéressant...
  $('#suppr-user').click(function(){
    supprimerUser(user)
  });

}


function DetailsUser(id){
  console.log("détails user ; "+id);
  $.ajax({
      url: "http://localhost:8000/PAL/api/v1.0/user/"+id,
      type: "GET",
      dataType : "json",
      success: function(rep, status, err) {
          console.log(JSON.stringify(rep));
          DomBuilderDetailsUser(id,rep);
        },
      error: function(rep, status, err) {
        $('#container').html("<b>Impossible de récupérer les informations utilisateurs !</b>");

        }
      });

  }

function DomBuilderDetailsUser(id, user){

  // Construit le DOM pour les détails de l'utilisateur avec à gauche les infos à jour, et à droite
  // les actions pour ajouter/supprimer des groupes, sessions, et jeux
  $('#detail').empty();

  $('#detail').append($('<div id="user-detail">'));
  $('#user-detail').append($('<p>').text('Pseudo : '+user.pseudo));
  $('#user-detail').append($('<p>').text('Email : '+user.email));
  $('#user-detail').append($('<p>').text('Lieu : '+user.lieu));
  $('#user-detail').append($('<p>').text('Description : '+user.description));
  $('#user-detail').append($('<p>').text('Privilèges : '+user.privileges));

  $('#user-detail').append($('<p>').text('Liste des jeux'));
  $('#user-detail').append($('<ul id="listeJeux">'));
  for (let i=0;i<user.listeJeux.length;i++){
    $('#listeJeux').append($('<li>').text(user.listeJeux[i].jeu_titre));
  }

  $('#user-detail').append($('<p>').text('Liste des groupes'));
  $('#user-detail').append($('<ul id="listeGroupes">'));
  for (let i=0;i<user.listeGroup.length;i++){
    $('#listeGroupes').append($('<li>').text(user.listeGroup[i].group_titre));
  }

  $('#user-detail').append($('<p>').text('Liste des Sessions'));
  $('#user-detail').append($('<ul id="listeSessions">'));
  for (let i=0;i<user.listeSession.length;i++){
    $('#listeSession').append($('<li>').text(user.listeSession[i].session_titre));
  }

  $('#detail').append($('<div id="user-action">'));
  $('#user-action').append($('<p>').text('Test, ici les action pour ajouter une session, un group, un jeu!'));
}


function ajouterUser(){
  $('#detail').empty();
  $('#detail').append($('<div id="form-ajout-user">'));
  $('#form-ajout-user').append($('<p>').text("Ajout d'un Membres :"));
  $('#form-ajout-user').append($('<label for="pseudo">').text("Pseudo :"));
  $('#form-ajout-user').append($('<input id="pseudo" name="pseudo">').attr("placeholder","Spike"));
  $('#form-ajout-user').append($('<label for="email">').text('votre Email :'));
  $('#form-ajout-user').append($('<input id="email" name="email">').attr("placeholder","cowBoy@bebop.com"));
  $('#form-ajout-user').append($('<label for="lieu">').text('Lieu de résidence :'));
  $('#form-ajout-user').append($('<input id="lieu"> name="lieu"').attr("placeholder","Marineris Valley"));
  $('#form-ajout-user').append($('<label for="description">').text('Décrivez-vous :'));
  $('#form-ajout-user').append($('<input id="description"> name="description"').attr("placeholder","Je suis le plus beau, le meilleur"));
  $('#form-ajout-user').append($('<button id="form-user-valid">').text("Valider"));

  $('#form-user-valid').click(function(){
    let data = new User(
      $('#pseudo').val(),
      $('#email').val(),
      $('#lieu').val(),
      $('#description').val()
    );

    $.ajax({
        url: "http://localhost:8000/PAL/api/v1.0/user",
        type: "POST",
        dataType : "json",
        data : JSON.stringify(data),
        success: function(rep, status, err) {
            console.log(JSON.stringify(rep));
            DomBuilderDetailsUser(rep.id,rep);
          },
        error: function(rep, status, err) {
          $('#container').html("<b>Impossible de récupérer les informations utilisateurs !</b>");

          }
        });
  });

}



function supprimerUser(user){
  $('#detail').empty();
  $('#detail').append($('<div id="form-suppr-user">'));
  $('#form-suppr-user').append($('<p>').text('Supprimer l\'utilisateur :'))
  .append($('<select id="select-user" name="select-user">'));
  for (let i=0;i<user.length;i++){
    $('#select-user').append($('<option>').text(user[i].pseudo).val(user[i].id));
  }
  $('#select-user').click(function(){
    var debile = $('#select-user').value;
    console.log(debile);
  });
}
