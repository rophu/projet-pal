class Session{
  constructor(emprunt_id,titre,description, nbMin, nbMax, duree,lieu,date,heureDebut,heureFin){
    this.emprunt_id = emprunt_id;
    this.titre = titre;
    this.description = description;
    this.nb_min = nbMin;
    this.nb_max = nbMax;
    this.duree = duree;
    this.lieu = lieu;
    this.date = date;
    this.heureDebut = heureDebut;
    this.heureFin = heureFin;
  }
}



$('#formCreerSession1').hide();
$('#formCreerSession2').hide();
$('#formCreerSession3').hide();
$('#formCreerSession4').hide();
$('#formCreerSession5').hide();


//lister les prochaines sessions
async function getSessions(){
  let rep = await fetch('http://localhost:8000/PAL/api/v1.0/session', { method: 'GET' });
  let reponse = await rep.json();
  return reponse;

}

function joinSession(reponse){
  $('#session').append($('<div class="jumbotron" id="rejoindreSessionContainer">'));
  $('#rejoindreSessionContainer').append($('<h2 class="display-3">Rejoindre une session</h2>'));
  $('#rejoindreSessionContainer').append($('<p class="lead" id="PrejoindreSessionContainer">'));
  $('#PrejoindreSessionContainer').append($('<a class="btn btn-success" href="#" role="button">Creer une nouvelle session</a>'));
  $('#rejoindreSessionContainer').append($('<table class="table table-hover" id="tablerejoindreSessionContainer">'));
  $('#tablerejoindreSessionContainer').append($('<thead id="headTablerejoindreSessionContainer">'));
  $('#tablerejoindreSessionContainer').append($('<tr id="enteteJoinSession" class="table-default">'));
  $('#enteteJoinSession').empty();
  $('#enteteJoinSession').append($('<th>').text("Nom"));
  $('#enteteJoinSession').append($('<th>').text("Jeu"));
  $('#enteteJoinSession').append($('<th>').text("responsable"));
  $('#enteteJoinSession').append($('<th>').text("Nombre de membres"));
  $('#enteteJoinSession').append($('<th>').text("date"));
  $('#enteteJoinSession').append($('<th>').text("Heure"));
  $('#enteteJoinSession').append($('<th>').text("Lieux"));
  $('#tablerejoindreSessionContainer').append($('<tbody id="containerJoinSession">'));
  $('#containerJoinSession').empty();
  for (let i=0;i<reponse.sessions.length;++i){
    $('#containerJoinSession').append($('<tr id=tdJoinSession'+i+' class="table-default">'));
    $('#tdJoinSession'+i).append($('<td>').text(reponse.sessions[i].titre));
    $('#tdJoinSession'+i).append($('<td>').text(reponse.sessions[i].jeu_titre));
    $('#tdJoinSession'+i).append($('<td>').text(reponse.sessions[i].emprunt_user));
    $('#tdJoinSession'+i).append($('<td>').text(reponse.sessions[i].nb_min+'/'+reponse.sessions[i].nb_max));
    $('#tdJoinSession'+i).append($('<td>').text(formatDateJour(reponse.sessions[i].date.date)));
    $('#tdJoinSession'+i).append($('<td>').text(formatDateHeure(reponse.sessions[i].heureDebut.date)));
    $('#tdJoinSession'+i).append($('<td>').text(reponse.sessions[i].lieu));
    $('#tdJoinSession'+i).append($('<td><button type="button" class="btn btn-info">Détails</button></td>'));
    $('#tdJoinSession'+i).append($('<td><button type="button" class="btn btn-success">Participer</button></td>'));
  }
}


  async function getMySessions(){
    let rep = await fetch('http://localhost:8000/PAL/api/v1.0/user/'+$("#idUser").val(), { method: 'GET' });
    let reponse = await rep.json();
    return reponse;
}
  function mySession(reponse){
    $('#session').append($('<div class="jumbotron" id="MySessionContainer">'));
    $('#MySessionContainer').append($('<h2 class="display-3">Mes sessions</h2>'));
    $('#MySessionContainer').append($('<p class="lead" id="PMySessionContainer">'));
    $('#PMySessionContainer').append($('<a class="btn btn-success" href="#" role="button">Creer une nouvelle session</a>'));
    $('#MySessionContainer').append($('<table class="table table-hover" id="tableMySessionContainer">'));
    $('#tableMySessionContainer').append($('<thead id="headTablerejoindreSessionContainer">'));
    $('#tableMySessionContainer').append($('<tr id="enteteMySession" class="table-default">'));
    $('#enteteMySession').empty();
    $('#enteteMySession').append($('<th>').text("Nom"));
    $('#enteteMySession').append($('<th>').text("Jeu"));
    $('#enteteMySession').append($('<th>').text("responsable"));
    $('#enteteMySession').append($('<th>').text("Nombre de membres"));
    $('#enteteMySession').append($('<th>').text("date"));
    $('#enteteMySession').append($('<th>').text("Heure"));
    $('#enteteMySession').append($('<th>').text("Lieux"));
    $('#tableMySessionContainer').append($('<tbody id="containerMySession">'));
    $('#containerMySession').empty();
    for (let i=0;i<reponse.listeSession.length;++i){
      $('#containerMySession').append($('<tr id=tdMySession'+i+' class="table-default">'));
      $('#tdMySession'+i).append($('<td>').text(reponse.listeSession[i].session_titre));
      $('#tdMySession'+i).append($('<td>').text(reponse.listeSession[i].session_titre_jeu));
      $('#tdMySession'+i).append($('<td>').text(reponse.listeSession[i].session_user));
      $('#tdMySession'+i).append($('<td>').text(formatDateJour(reponse.listeSession[i].session_date.date)));
      $('#tdMySession'+i).append($('<td>').text(formatDateHeure(reponse.listeSession[i].session_heuredebut.date)));
      $('#tdMySession'+i).append($('<td>').text(reponse.listeSession[i].session_lieu));
      $('#tdMySession'+i).append($('<td><button type="button" class="btn btn-info">Détails</button></td>'));
      $('#tdMySession'+i).append($('<td><button type="button" class="btn btn-warning">Se désinsrire</button></td>'));
      $('#tdMySession'+i).append($('<td><button type="button" class="btn btn-danger">Supprimer</button></td>'));


    }
}

async function postSession(){
  var dateSession = new Date($("#inputCreationDateSession").val());
  var heureDebut = new Date("1970-01-01 "+$("#inputCreationDebutSession").val());
  var heureFin   = new Date("1970-01-01 "+$("#inputCreationFinSession").val());
  var duree      = (heureFin.getHours()*60+heureFin.getMinutes())-(heureDebut.getHours()*60+heureDebut.getMinutes());
  console.log($("#laRuse").val());
    var session = new Session(
        $("#laRuse").val(),
        $("#inputCreationNomSession").val(),
        $("#descriptionSessionTextarea").val(),
        $("#inputCreationMinSession").val(),
        $("#inputCreationMaxSession").val(),
        duree,
        $("#inputCreationLieuSession").val(),
        dateSession,
        heureDebut,
        heureFin
        );
    console.log(session);
    rep = await fetch('http://localhost:8000/PAL/api/v1.0/session',{
      header: {
        'Accept':'application/json',
        'Content-Type' : 'application/json'
      },
     method: 'POST',
     body: JSON.stringify(session)});
    await rep.json();
    try {
       const reponse = await rep.json();
       return reponse;
     } catch (err) {
       throw err;
     }
}
function formatDateJour(s){
      var months = ['01','02','03','04','05','06',
                    '07','08','09','10','11','12'];
      var b = s.split(/\D+/);
      var d = new Date(b[0],--b[1],b[2],b[3],b[5],b[5]);
      return d.getDate() + '/' + months[d.getMonth()] + '/' + d.getFullYear();
     }

function formatDateHeure(s){
    function z(n){return (n<10?'0':'')+n}
    var months = ['01','02','03','04','05','06',
                  '07','08','09','10','11','12'];
    var b = s.split(/\D+/);
    var d = new Date(b[0],--b[1],b[2],b[3],b[5],b[5]);
    return z(d.getHours()%24 || 24) + ':' + z(d.getMinutes());
    }


function creerListeSession(){
  getSessions().then(reponse=>joinSession(reponse));
}
function creerMaListeSession(){
  getMySessions().then(reponse=>mySession(reponse));
}
function appelPostFinal(){
  postSession().then($('#formCreerSession5').empty());
}
function creerFormSession1(){
  $('#buttonCreerSession').hide();
  $('#formCreerSession1').show();
}
function creerFormSession2(){
  $('#formCreerSession1').hide();
  $('#formCreerSession2').show();

}
function creerFormSession3(){
  $('#formCreerSession2').hide();
  $('#formCreerSession3').show();
}
function creerFormSession4(){
  $('#formCreerSession3').hide();
  $('#formCreerSession4').show();
  let titre = ($("#selectCreerListeJeu").val())[0];
  getDetailsJeuByTitre(titre).then(reponse=>postEmprunt(reponse)).then(reponse=>{
    $('#formCreerSession3').append($('<input id="laRuse">').val(reponse.id))});
}
function creerFormSession5(){
  $('#formCreerSession4').hide();
  $('#formCreerSession5').show();
}
