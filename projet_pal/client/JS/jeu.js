class Jeu{
  constructor(titre,auteur,editeur,anneeEdition,dureePartie,type,complexite,nbMin,nbMax,description){
    this.titre = titre;
    this.auteur = auteur;
    this.editeur = editeur;
    this.anneeEdition = anneeEdition;
    this.dureePartie = dureePartie;
    this.type = type;
    this.complexite = complexite;
    this.nbMin = nbMin;
    this.nbMax = nbMax;
    this.description = description;
  }
}

class supprJeu{
  constructor(id){
    this.id = id;
  }
}

$('#tableauListeJeux').hide();
$('#containerDetails').hide();


  listeJeu().then(reponse=>creerListeJeu(reponse));

  //Cette fonction permet de récupérer les données d'un jeu
  //en JSON
  async function listeJeu(){
    let rep = await fetch('http://localhost:8000/PAL/api/v1.0/jeu', { method: 'GET' });
    let reponse = await rep.json();
    return reponse;
  }

  //Cette fonction permet de récupérer les données d'un jeu
  // grâce à son titre en JSON
  async function getDetailsJeuByTitre(titre){

    let rep = await fetch('http://localhost:8000/PAL/api/v1.0/jeu/details/'+titre, { method: 'GET' });
    let reponse = await rep.json();
    return reponse;
  }

  //Cette fonction permet de récupérer les données d'un jeu
  // grâce à son id en JSON
  async function getDetailsJeuById(id){

    let rep = await fetch('http://localhost:8000/PAL/api/v1.0/jeu/'+id, { method: 'GET' });
    let reponse = await rep.json();
    return reponse;
  }

async function creerModifierJeux(){
  var jeu = new Jeu(
    $("#inputFormDetailTitre").val(),
    $("#inputFormDetailAuteur").val(),
    $("#inputFormDetailEditeur").val(),
    $("#inputFormDetailAnneeEdition").val(),
    $("#inputFormDetailDureePartie").val(),
    $("#inputFormDetailType").val(),
    $("#inputFormDetailComplexite").val(),
    $("#inputFormDetailNbMin").val(),
    $("#inputFormDetailNbMax").val(),
    $("#inputFormDescription").text()
  );
  var idJeu = $("#idCache").val();
  rep = await fetch('http://localhost:8000/PAL/api/v1.0/jeu/'+idJeu,{
    header: {
      'Accept':'application/json',
      'Content-Type' : 'application/json'
    },
   method: 'PUT',
   body: JSON.stringify(jeu)});
  await rep.json();
  try {
     const reponse = await rep.json();
     return reponse;
   } catch (err) {
     throw err;
   }

}
async function envoiSupprimerJeu(){
  var id = new supprJeu(
    $("#idCache").val()
  );
  rep = await fetch('http://localhost:8000/PAL/api/v1.0/jeu',{
    header: {
      'Accept':'application/json',
      'Content-Type' : 'application/json'
    },
   method: 'DELETE',
   body: JSON.stringify(id)});
  await rep.json();
  try {
     const reponse = await rep.json();
     return reponse;
   } catch (err) {
     throw err;
   }
}

  //Cette fonction construit un liste des jeux Dans
  //le DOM
  function creerListeJeu(reponse){
    for (let i=0;i<reponse.jeu.length;++i){
      $('#selectCreerListeJeu').append($('<option id=optionListeJeu'+i+' ondblclick=detailsClickJeu('+reponse.jeu[i].id+') class="table-default">'));
      $('#optionListeJeu'+i).append($('<p>').text(reponse.jeu[i].titre));
    }
  }

  //Cette fonction construit un liste des jeux Dans
  //le DOM
  function getListeJeux(reponse){
    $('#tableauListeJeux').show();
    $('#containerListeJeux').empty();
    for (let i=0;i<reponse.jeu.length;++i){
      $('#containerListeJeux').append($('<tr id=tdListeJeux'+i+' class="table-secondary">'));
      $('#tdListeJeux'+i).append($('<td>').text(reponse.jeu[i].titre));
      $('#tdListeJeux'+i).append($('<td>').text(reponse.jeu[i].type));
      $('#tdListeJeux'+i).append($('<td>').text(reponse.jeu[i].complexite));
      $('#tdListeJeux'+i).append($('<td>').text(reponse.jeu[i].nbMin));
      $('#tdListeJeux'+i).append($('<td>').text(reponse.jeu[i].nbMax));
      $('#tdListeJeux'+i).append($('<td>').text(reponse.jeu[i].dureePartie));
      $('#tdListeJeux'+i).append($('<td><a role="button" href="#detailsPageJeu" onclick="getPageDetailsJeu('+reponse.jeu[i].id+')" class="btn btn-info">Détails</a></td>'));
      $('#tdListeJeux'+i).append($('<td><a role="button" class="btn btn-success">Ajouter à ma collection</a></td>'));
    }
  }
  //Cette fonction construit un popup construit à partir des données
  //JSON d'un jeu.
  function carteDetailsJeu(reponse){
    $('#alertCreerSessionContainer').empty();
    $('#creerSessionContainer').append($('<div id="alertCreerSessionContainer">'));
    $('#alertCreerSessionContainer').append($('<div role="alert" class="alert alert-dismissible alert-primary" id="alertdetailsJeu">'));
    $('#alertdetailsJeu').append($('<button type="button" class="close" data-dismiss="alert">&times;</button>'));
    $('#alertdetailsJeu').append($('<p>').text('titre : '+reponse.titre));
    $('#alertdetailsJeu').append($('<p>').text('Edition : '+reponse.anneeEdition));
    $('#alertdetailsJeu').append($('<p>').text('nb joueurs min/max : '+reponse.nbMin+'/'+reponse.nbMax));
    $('#alertdetailsJeu').append($('<p>').text('Duree : '+reponse.dureePartie));
    $('#alertdetailsJeu').append($('<p>').text('Complexite : '+reponse.complexite));
    $('#alertdetailsJeu').append($('<p>').text('type : '+reponse.type));
  }
function creerPageDetailsJeu(reponse){
  $('#containerDetails').show();
  $('#formDetailTitre').empty();
  $('#formDetailTitre').append($('<label for="inputFormDetailTitre">').text('Titre'));
  $('#formDetailTitre').append($('<input type="text" id="inputFormDetailTitre" class="form-control" >').val(reponse.titre));
  $('#formDetailComplexite').empty();
  $('#formDetailComplexite').append($('<label for="inputFormDetailComplexite">').text('Complexite'));
  $('#formDetailComplexite').append($('<input type="text" id="inputFormDetailComplexite" class="form-control" >').val(reponse.complexite));
  $('#formDetailType').empty();
  $('#formDetailType').append($('<label for="inputFormDetailType">').text('Type'));
  $('#formDetailType').append($('<input type="text" id="inputFormDetailType" class="form-control" >').val(reponse.type));
  $('#formDetailAuteur').empty();
  $('#formDetailAuteur').append($('<label for="inputFormDetailAuteur">').text('Auteur'));
  $('#formDetailAuteur').append($('<input type="text" id="inputFormDetailAuteur" class="form-control" >').val(reponse.auteur));
  $('#formDetailEditeur').empty();
  $('#formDetailEditeur').append($('<label for="inputFormDetailEditeur">').text('Editeur'));
  $('#formDetailEditeur').append($('<input type="text" id="inputFormDetailEditeur" class="form-control" >').val(reponse.editeur));
  $('#formDetailAnneeEdition').empty();
  $('#formDetailAnneeEdition').append($('<label for="inputFormDetailAnneeEdition">').text('Annee edition'));
  $('#formDetailAnneeEdition').append($('<input type="text" id="inputFormDetailAnneeEdition" class="form-control" >').val(reponse.anneeEdition));
  $('#formDetailDureePartie').empty();
  $('#formDetailDureePartie').append($('<label for="inputFormDetailDureePartie">').text('Duree partie'));
  $('#formDetailDureePartie').append($('<input type="text" id="inputFormDetailDureePartie" class="form-control" >').val(reponse.dureePartie));
  $('#formDetailNbMin').empty();
  $('#formDetailNbMin').append($('<label for="inputFormDetailNbMin">').text('nb joueurs min'));
  $('#formDetailNbMin').append($('<input type="text" id="inputFormDetailNbMin" class="form-control" >').val(reponse.nbMin));
  $('#formDetailNbMax').empty();
  $('#formDetailNbMax').append($('<label for="inputFormDetailNbMax">').text('nb joueurs max'));
  $('#formDetailNbMax').append($('<input type="text" id="inputFormDetailNbMax" class="form-control" >').val(reponse.nbMax));
  $('#formDetailDescription').empty();
  $('#formDetailDescription').append($('<label for="inputFormDescription">').text('description'));
  $('#formDetailDescription').append($('<textarea class="form-control" id="exampleFormControlTextarea1">').text(reponse.description));
  $('#formDetailDescription').append($('<input type="text" id="idCache" class="form-control" >').val(reponse.id));
  $('#idCache').hide();
}

//cette fonction permet le déclenchement de la fonction
//asynchrone creerModifierJeux()
function modifierJeux(){
  creerModifierJeux();
}

function detailsListeJeu(){
  listeJeu().then(reponse=>getListeJeux(reponse));
}

//cette fonction permet d'obtenir les détails d'un jeu sous forme d'un popup lors de la création
//d'une session
function detailsClickJeu(id){
  getDetailsJeuById(id).then(reponse=>carteDetailsJeu(reponse));
}

//Cette fonction permet de récupérer les données d'un jeu
// grâce à son id en JSON
function getPageDetailsJeu(idJeu){
  getDetailsJeuById(idJeu).then(reponse=>creerPageDetailsJeu(reponse));
}

//cette fonction permet le déclenchement de la fonction
//asynchrone envoiSupprimerJeu()
function supprimerJeu(){
  envoiSupprimerJeu();
}

function getJeuId(reponse){
  var id = String(reponse.jeu[0].id);
  return id;
}
