class Emprunt{
  constructor(jeu_id,user_id,dateEmprunt,dateRetour){
    this.jeu_id = jeu_id;
    this.user_id = user_id;
    this.dateEmprunt = dateEmprunt;
    this.dateRetour = dateRetour;
  }
}


  async function postEmprunt(reponse){
    var id = reponse.jeu[0].id;
    var dateDebutEmpruntForm = new Date($("#inputCreationDebutEmprunt").val());
    var dateFinEmpruntForm = new Date($("#inputCreationRetourEmprunt").val());
    console.log(dateDebutEmpruntForm);
    var session = new Emprunt(
        id,
        ($("#idUser").val()),
        dateDebutEmpruntForm,
        dateFinEmpruntForm
        );
    rep = await fetch('http://localhost:8000/PAL/api/v1.0/emprunt',{
      header: {
        'Accept':'application/json',
        'Content-Type' : 'application/json'
      },
     method: 'POST',
     body: JSON.stringify(session)});
     console.log(session);
     try {
        const reponse = await rep.json();
        return reponse;
      } catch (err) {
        throw err;
      }
  }
