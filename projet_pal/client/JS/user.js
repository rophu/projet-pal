class User{
  constructor(pseudo, email, lieu, description){
    this.pseudo = pseudo;
    this.email = email;
    this.lieu = lieu;
    this.description = description;
    this.privileges = "aucun";
  }
}



$('#inscriptionForm').hide();
$('#siteBody').hide();
$("#buttonUser").click(getUsers);


//liste tous les users de la base
async function getUsers(){
    let rep = await fetch('http://localhost:8000/PAL/api/v1.0/user', { method: 'GET' });
    let reponse = await rep.json();
    console.log(reponse);
    $('#enteteUser').empty();
    $('#enteteUser').append($('<th>').text("Pseudo"));
    $('#enteteUser').append($('<th>').text("mail"));
    $('#enteteUser').append($('<th>').text("description"));
    $('#containerUser').empty();
    for (let i=0;i<reponse.user.length;++i){
      $('#containerUser').append($('<tr id='+i+' class="table-success">'));
      $('#'+i).append($('<td>').text(reponse.user[i].pseudo));
      $('#'+i).append($('<td>').text(reponse.user[i].email));
      $('#'+i).append($('<td>').text(reponse.user[i].description));
    }
}
async function getUserbyPseudo(){
  let rep = await fetch('http://localhost:8000/PAL/api/v1.0/user/details/'+$("#inputPseudoC").val(), { method: 'GET' });
  let reponse = await rep.json();
  console.log(reponse);
  return reponse;

}

async function postUser(){
  var user = new User(
      $("#inputPseudoInscription").val(),
      $("#inputMailInscription").val(),
      $("#inputAdresseInscription").val()+$("#inputVilleInscription").val(),
      $("#inputPrenomInscription").val()+$("#inputNomInscription").val()
      );
  rep = await fetch('http://localhost:8000/PAL/api/v1.0/user',{
    header: {
      'Accept':'application/json',
      'Content-Type' : 'application/json'
    },
   method: 'POST',
   body: JSON.stringify(user)});
   try {
      const reponse = await rep.json();
      return reponse;
    } catch (err) {
      throw err;
    }
}

function connexionSite(){
  getUserbyPseudo().then(reponse=>boxPseudoConnexion(reponse))
}

  function envoiInscription(){
    postUser().then(reponse=>boxPseudo(reponse));
  }
function formInsOn(){
  $('#inscriptionForm').show();
  $('#formConnexion').hide();
}

function boxPseudo(reponse){
  $('#footerSite').append($('<input type="text" id="idUser" class="form-control" >').val(reponse.id));
  $('#idUser').hide();
  $('#siteBody').show();
  $('#inscriptionForm').hide();

}
function boxPseudoConnexion(reponse){
  $('#footerSite').append($('<input type="text" id="idUser" class="form-control" >').val(reponse.id));
  $('#idUser').hide();
  $('#siteBody').show();
  $('#formConnexion').hide();
}

function traitementDataUser(){
  let reponse = getUsers();
  $('#taches').empty();
  $('#taches').append($('<ul>'));
  console.log(reponse);
  for(var i=0;i<reponse.length;i++){
    console.log(reponse[i]);
    /*$('#taches ul')
      .append($('<li>'))
      .append($('<a>'))
      .text(reponse[i].pseudo).on("click",reponse[i], details);*/
  }
}
function onerror(err){
  $("#taches").html("<b>Impossible de récupérer les taches à réaliser !<b>"+err);
}

function details(event){
    $("#currenttask").empty();
    formTask();
    fillFormTask(event.data);
  }
  /* Objet User en JS
  class User{
      constructor(title, description, done, uri){
          this.title = title;
          this.description = description;
          this.done = done;
          this.uri = uri;
          console.log(this.uri);
      }
    }*/


//donne les détails d'un user par son id
/*async function userById(id){
    let rep = await fetch('http://localhost:8000/PAL/api/v1.0/user/'+id, { method: 'GET', body: JSON.stringify({ "id": id })});
    let reponse = await rep.json();
    return reponse;
}*/


//supprime un user grâce à son id
/*async function userSupprime(id){
    let rep = await fetch('http://localhost:8000/PAL/api/v1.0/user/',
     { method: 'DELETE', body: JSON.stringify({ "id": id })
    });
    let reponse = await rep.json();
    return reponse;
}*/
