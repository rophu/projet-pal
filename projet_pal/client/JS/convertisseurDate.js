$(function(){
  console.log(formatDateJour('2014-01-30 16:21:09'));
  console.log(formatDateHeure('2014-01-30 16:21:09'));
  function formatDateJour(s){
        var months = ['01','02','03','04','05','06',
                      '07','08','09','10','11','12'];
        var b = s.split(/\D+/);
        var d = new Date(b[0],--b[1],b[2],b[3],b[5],b[5]);
        return d.getDate() + '/' + months[d.getMonth()] + '/' + d.getFullYear();
       }

  function formatDateHeure(s){
      function z(n){return (n<10?'0':'')+n}
      var months = ['01','02','03','04','05','06',
                    '07','08','09','10','11','12'];
      var b = s.split(/\D+/);
      var d = new Date(b[0],--b[1],b[2],b[3],b[5],b[5]);
      return z(d.getHours()%24 || 24) + ':' + z(d.getMinutes());
      }
});
