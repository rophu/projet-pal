<?php

namespace App\Entity;

use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass=GroupRepository::class)
 * @ORM\Table(name="`group`")
 */
class Group
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $titre;

    // /**
    //  * @ORM\Column(type="string", length=255)
    //  */
    // private $Password;

    /**
     * @ORM\Column(type="text")
     */
    private $Description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Public;

    /**
     * @ORM\OneToMany(targetEntity=Inclure::class, mappedBy="Grup", orphanRemoval=true)
     */
    private $inclures;

    public function __construct()
    {
        $this->inclures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    // public function getPassword(): ?string
    // {
    //     return $this->Password;
    // }
    //
    // public function setPassword(string $Password): self
    // {
    //     $this->Password = $Password;
    //
    //     return $this;
    // }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->Public;
    }

    public function setPublic(bool $Public): self
    {
        $this->Public = $Public;

        return $this;
    }

    /**
     * @return Collection|Inclure[]
     */
    public function getInclures(): Collection
    {
        return $this->inclures;
    }

    public function addInclure(Inclure $inclure): self
    {
        if (!$this->inclures->contains($inclure)) {
            $this->inclures[] = $inclure;
            $inclure->setGrup($this);
        }

        return $this;
    }

    public function removeInclure(Inclure $inclure): self
    {
        if ($this->inclures->contains($inclure)) {
            $this->inclures->removeElement($inclure);
            // set the owning side to null (unless already changed)
            if ($inclure->getGrup() === $this) {
                $inclure->setGrup(null);
            }
        }

        return $this;
    }
}
