<?php

namespace App\Entity;

use App\Repository\EmprunterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmprunterRepository::class)
 */
class Emprunter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $DateEmprunt;

    /**
     * @ORM\Column(type="date")
     */
    private $DateRetour;

    // /**
    //  * @ORM\OneToOne(targetEntity=Session::class, cascade={"persist", "remove"})
    //  * @ORM\JoinColumn(nullable=false)
    //  */
    // private $Session;

    /**
     * @ORM\ManyToOne(targetEntity=Jeu::class, inversedBy="listeEmprunt")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Jeu;

    /**
     * @ORM\OneToMany(targetEntity=Session::class, mappedBy="Emprunt", orphanRemoval=true)
     */
    private $sessions;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="emprunters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateEmprunt(): ?\DateTimeInterface
    {
        return $this->DateEmprunt;
    }

    public function setDateEmprunt(\DateTimeInterface $DateEmprunt): self
    {
        $this->DateEmprunt = $DateEmprunt;

        return $this;
    }

    public function getDateRetour(): ?\DateTimeInterface
    {
        return $this->DateRetour;
    }

    public function setDateRetour(\DateTimeInterface $DateRetour): self
    {
        $this->DateRetour = $DateRetour;

        return $this;
    }

    // public function getSession(): ?Session
    // {
    //     return $this->Session;
    // }
    //
    // public function setSession(Session $Session): self
    // {
    //     $this->Session = $Session;
    //
    //     return $this;
    // }

    public function getJeu(): ?Jeu
    {
        return $this->Jeu;
    }

    public function setJeu(?Jeu $Jeu): self
    {
        $this->Jeu = $Jeu;

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setEmprunt($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->contains($session)) {
            $this->sessions->removeElement($session);
            // set the owning side to null (unless already changed)
            if ($session->getEmprunt() === $this) {
                $session->setEmprunt(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
