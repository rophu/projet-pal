<?php

namespace App\Entity;

use App\Repository\JeuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JeuRepository::class)
 */
class Jeu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $auteur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $editeur;

    /**
     * @ORM\Column(type="integer")
     */
    private $anneeEdition;

    /**
     * @ORM\Column(type="integer")
     */
    private $dureePartie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $complexite;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbMin;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbMax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    // /**
    //  * @ORM\Column(type="integer")
    //  */
    // private $anneeAcquisition;

    // /**
    //  * @ORM\Column(type="string", length=255)
    //  */
    // private $etat;

    // /**
    //  * @ORM\Column(type="float")
    //  */
    // private $prix;

    // /**
    //  * @ORM\OneToMany(targetEntity=Jouer::class, mappedBy="Jeu")
    //  */
    // private $jouers;

    /**
     * @ORM\OneToMany(targetEntity=Posseder::class, mappedBy="Jeu")
     */
    private $UserPossedantJeu;

    /**
     * @ORM\OneToMany(targetEntity=Emprunter::class, mappedBy="Jeu")
     */
    private $listeEmprunt;

    public function __construct()
    {
        //$this->jouers = new ArrayCollection();
        $this->UserPossedantJeu = new ArrayCollection();
        $this->listeEmprunt = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getEditeur(): ?string
    {
        return $this->editeur;
    }

    public function setEditeur(string $editeur): self
    {
        $this->editeur = $editeur;

        return $this;
    }

    public function getAnneeEdition(): ?int
    {
        return $this->anneeEdition;
    }

    public function setAnneeEdition(int $anneeEdition): self
    {
        $this->anneeEdition = $anneeEdition;

        return $this;
    }

    public function getDureePartie(): ?int
    {
        return $this->dureePartie;
    }

    public function setDureePartie(int $dureePartie): self
    {
        $this->dureePartie = $dureePartie;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getComplexite(): ?int
    {
        return $this->complexite;
    }

    public function setComplexite(int $complexite): self
    {
        $this->complexite = $complexite;

        return $this;
    }

    public function getNbMin(): ?int
    {
        return $this->nbMin;
    }

    public function setNbMin(int $nbMin): self
    {
        $this->nbMin = $nbMin;

        return $this;
    }

    public function getNbMax(): ?int
    {
        return $this->nbMax;
    }

    public function setNbMax(int $nbMax): self
    {
        $this->nbMax = $nbMax;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    // public function getAnneeAcquisition(): ?int
    // {
    //     return $this->anneeAcquisition;
    // }
    //
    // public function setAnneeAcquisition(int $anneeAcquisition): self
    // {
    //     $this->anneeAcquisition = $anneeAcquisition;
    //
    //     return $this;
    // }

    // public function getEtat(): ?string
    // {
    //     return $this->etat;
    // }
    //
    // public function setEtat(string $etat): self
    // {
    //     $this->etat = $etat;
    //
    //     return $this;
    // }

    // public function getPrix(): ?float
    // {
    //     return $this->prix;
    // }
    //
    // public function setPrix(float $prix): self
    // {
    //     $this->prix = $prix;
    //
    //     return $this;
    // }

    // /**
    //  * @return Collection|Jouer[]
    //  */
    // public function getJouers(): Collection
    // {
    //     return $this->jouers;
    // }

    // public function addJouer(Jouer $jouer): self
    // {
    //     if (!$this->jouers->contains($jouer)) {
    //         $this->jouers[] = $jouer;
    //         $jouer->setJeu($this);
    //     }
    //
    //     return $this;
    // }
    //
    // public function removeJouer(Jouer $jouer): self
    // {
    //     if ($this->jouers->contains($jouer)) {
    //         $this->jouers->removeElement($jouer);
    //         // set the owning side to null (unless already changed)
    //         if ($jouer->getJeu() === $this) {
    //             $jouer->setJeu(null);
    //         }
    //     }
    //
    //     return $this;
    // }

    /**
     * @return Collection|Posseder[]
     */
    public function getUserPossedantJeu(): Collection
    {
        return $this->UserPossedantJeu;
    }

    public function addUserPossedantJeu(Posseder $userPossedantJeu): self
    {
        if (!$this->UserPossedantJeu->contains($userPossedantJeu)) {
            $this->UserPossedantJeu[] = $userPossedantJeu;
            $userPossedantJeu->setJeu($this);
        }

        return $this;
    }

    public function removeUserPossedantJeu(Posseder $userPossedantJeu): self
    {
        if ($this->UserPossedantJeu->contains($userPossedantJeu)) {
            $this->UserPossedantJeu->removeElement($userPossedantJeu);
            // set the owning side to null (unless already changed)
            if ($userPossedantJeu->getJeu() === $this) {
                $userPossedantJeu->setJeu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Emprunter[]
     */
    public function getListeEmprunt(): Collection
    {
        return $this->listeEmprunt;
    }

    public function addListeEmprunt(Emprunter $listeEmprunt): self
    {
        if (!$this->listeEmprunt->contains($listeEmprunt)) {
            $this->listeEmprunt[] = $listeEmprunt;
            $listeEmprunt->setJeu($this);
        }

        return $this;
    }

    public function removeListeEmprunt(Emprunter $listeEmprunt): self
    {
        if ($this->listeEmprunt->contains($listeEmprunt)) {
            $this->listeEmprunt->removeElement($listeEmprunt);
            // set the owning side to null (unless already changed)
            if ($listeEmprunt->getJeu() === $this) {
                $listeEmprunt->setJeu(null);
            }
        }

        return $this;
    }
}
