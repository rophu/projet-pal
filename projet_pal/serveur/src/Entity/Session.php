<?php

namespace App\Entity;

use App\Repository\SessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SessionRepository::class)
 */
class Session
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $Description;

    /**
     * @ORM\Column(type="integer")
     */
    private $NbMin;

    /**
     * @ORM\Column(type="integer")
     */
    private $NbMax;

    /**
     * @ORM\Column(type="integer")
     */
    private $Duree;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Lieu;

    /**
     * @ORM\Column(type="date")
     */
    private $Date;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="sessions")
     */
    private $Participer;

    /**
     * @ORM\Column(type="time")
     */
    private $heuredebut;

    /**
     * @ORM\Column(type="time")
     */
    private $heurefin;

    /**
     * @ORM\ManyToOne(targetEntity=Emprunter::class, inversedBy="sessions")
     * @ORM\JoinColumn(nullable=true)
     */
    private $Emprunt;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $titre;

    public function __construct()
    {
        $this->Participer = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getNbMin(): ?int
    {
        return $this->NbMin;
    }

    public function setNbMin(int $NbMin): self
    {
        $this->NbMin = $NbMin;

        return $this;
    }

    public function getNbMax(): ?int
    {
        return $this->NbMax;
    }

    public function setNbMax(int $NbMax): self
    {
        $this->NbMax = $NbMax;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->Duree;
    }

    public function setDuree(int $Duree): self
    {
        $this->Duree = $Duree;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->Lieu;
    }

    public function setLieu(string $Lieu): self
    {
        $this->Lieu = $Lieu;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getParticiper(): Collection
    {
        return $this->Participer;
    }

    public function addParticiper(User $participer): self
    {
        if (!$this->Participer->contains($participer)) {
            $this->Participer[] = $participer;
        }

        return $this;
    }

    public function removeParticiper(User $participer): self
    {
        if ($this->Participer->contains($participer)) {
            $this->Participer->removeElement($participer);
        }

        return $this;
    }

    public function getHeuredebut(): ?\DateTimeInterface
    {
        return $this->heuredebut;
    }

    public function setHeuredebut(\DateTimeInterface $heuredebut): self
    {
        $this->heuredebut = $heuredebut;

        return $this;
    }

    public function getHeurefin(): ?\DateTimeInterface
    {
        return $this->heurefin;
    }

    public function setHeurefin(\DateTimeInterface $heurefin): self
    {
        $this->heurefin = $heurefin;

        return $this;
    }

    public function getEmprunt(): ?Emprunter
    {
        return $this->Emprunt;
    }

    public function setEmprunt(?Emprunter $Emprunt): self
    {
        $this->Emprunt = $Emprunt;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }
}
