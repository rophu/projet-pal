<?php

namespace App\Entity;

use App\Repository\InclureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InclureRepository::class)
 */
class Inclure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="inclures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="inclures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Grup;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Responsable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getGrup(): ?Group
    {
        return $this->Grup;
    }

    public function setGrup(?Group $Grup): self
    {
        $this->Grup = $Grup;

        return $this;
    }

    public function getResponsable(): ?bool
    {
        return $this->Responsable;
    }

    public function setResponsable(bool $Responsable): self
    {
        $this->Responsable = $Responsable;

        return $this;
    }
}
