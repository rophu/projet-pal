<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $pseudo;

    // /**
    //  * @ORM\Column(type="string", length=255)
    //  */
    // private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lieu;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $privileges;

    /**
     * @ORM\ManyToMany(targetEntity=Session::class, mappedBy="Participer")
     */
    private $sessions;

    /**
     * @ORM\OneToMany(targetEntity=Inclure::class, mappedBy="User", orphanRemoval=true)
     */
    private $inclures;

    // /**
    //  * @ORM\OneToOne(targetEntity=Jouer::class, mappedBy="User", cascade={"persist", "remove"})
    //  */
    // private $jouer;

    /**
     * @ORM\OneToMany(targetEntity=Posseder::class, mappedBy="User")
     */
    private $jeuxPossedes;

    /**
     * @ORM\OneToMany(targetEntity=Emprunter::class, mappedBy="user", orphanRemoval=true)
     */
    private $emprunters;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->inclures = new ArrayCollection();
        $this->jeuxPossedes = new ArrayCollection();
        $this->emprunters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    // public function getPassword(): ?string
    // {
    //     return $this->password;
    // }
    //
    // public function setPassword(string $password): self
    // {
    //     $this->password = $password;
    //
    //     return $this;
    // }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrivileges(): ?string
    {
        return $this->privileges;
    }

    public function setPrivileges(string $privileges): self
    {
        $this->privileges = $privileges;

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->addParticiper($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->contains($session)) {
            $this->sessions->removeElement($session);
            $session->removeParticiper($this);
        }

        return $this;
    }

    /**
     * @return Collection|Inclure[]
     */
    public function getInclures(): Collection
    {
        return $this->inclures;
    }

    public function addInclure(Inclure $inclure): self
    {
        if (!$this->inclures->contains($inclure)) {
            $this->inclures[] = $inclure;
            $inclure->setUser($this);
        }

        return $this;
    }

    public function removeInclure(Inclure $inclure): self
    {
        if ($this->inclures->contains($inclure)) {
            $this->inclures->removeElement($inclure);
            // set the owning side to null (unless already changed)
            if ($inclure->getUser() === $this) {
                $inclure->setUser(null);
            }
        }

        return $this;
    }

    // public function getJouer(): ?Jouer
    // {
    //     return $this->jouer;
    // }

    // public function setJouer(Jouer $jouer): self
    // {
    //     $this->jouer = $jouer;
    //
    //     // set the owning side of the relation if necessary
    //     if ($jouer->getUser() !== $this) {
    //         $jouer->setUser($this);
    //     }
    //
    //    return $this;
    //}

    /**
     * @return Collection|Posseder[]
     */
    public function getJeuxPossedes(): Collection
    {
        return $this->jeuxPossedes;
    }

    public function addJeuxPossede(Posseder $jeuxPossede): self
    {
        if (!$this->jeuxPossedes->contains($jeuxPossede)) {
            $this->jeuxPossedes[] = $jeuxPossede;
            $jeuxPossede->setUser($this);
        }

        return $this;
    }

    public function removeJeuxPossede(Posseder $jeuxPossede): self
    {
        if ($this->jeuxPossedes->contains($jeuxPossede)) {
            $this->jeuxPossedes->removeElement($jeuxPossede);
            // set the owning side to null (unless already changed)
            if ($jeuxPossede->getUser() === $this) {
                $jeuxPossede->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Emprunter[]
     */
    public function getEmprunters(): Collection
    {
        return $this->emprunters;
    }

    public function addEmprunter(Emprunter $emprunter): self
    {
        if (!$this->emprunters->contains($emprunter)) {
            $this->emprunters[] = $emprunter;
            $emprunter->setUser($this);
        }

        return $this;
    }

    public function removeEmprunter(Emprunter $emprunter): self
    {
        if ($this->emprunters->contains($emprunter)) {
            $this->emprunters->removeElement($emprunter);
            // set the owning side to null (unless already changed)
            if ($emprunter->getUser() === $this) {
                $emprunter->setUser(null);
            }
        }

        return $this;
    }
}
