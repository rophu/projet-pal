<?php

namespace App\Entity;

use App\Repository\PossederRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PossederRepository::class)
 */
class Posseder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Niveau;

    /**
     * @ORM\Column(type="integer")
     */
    private $Tuteur;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_exemplaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $preferences;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $AnneeAcquisition;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Etat;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="jeuxPossedes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity=Jeu::class, inversedBy="UserPossedantJeu")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Jeu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNiveau(): ?int
    {
        return $this->Niveau;
    }

    public function setNiveau(int $Niveau): self
    {
        $this->Niveau = $Niveau;

        return $this;
    }

    public function getTuteur(): ?int
    {
        return $this->Tuteur;
    }

    public function setTuteur(int $Tuteur): self
    {
        $this->Tuteur = $Tuteur;

        return $this;
    }

    public function getNbExemplaire(): ?int
    {
        return $this->nb_exemplaire;
    }

    public function setNbExemplaire(int $nb_exemplaire): self
    {
        $this->nb_exemplaire = $nb_exemplaire;

        return $this;
    }

    public function getPreferences(): ?string
    {
        return $this->preferences;
    }

    public function setPreferences(string $preferences): self
    {
        $this->preferences = $preferences;

        return $this;
    }

    public function getAnneeAcquisition(): ?int
    {
        return $this->AnneeAcquisition;
    }

    public function setAnneeAcquisition(?int $AnneeAcquisition): self
    {
        $this->AnneeAcquisition = $AnneeAcquisition;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->Etat;
    }

    public function setEtat(?string $Etat): self
    {
        $this->Etat = $Etat;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getJeu(): ?Jeu
    {
        return $this->Jeu;
    }

    public function setJeu(?Jeu $Jeu): self
    {
        $this->Jeu = $Jeu;

        return $this;
    }
}
