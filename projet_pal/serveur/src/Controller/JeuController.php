<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Jeu;

function jeuReponseBuilder($jeu){
  $reponse = array(
  'id'               => $jeu->getId(),
  'titre'            => $jeu->getTitre(),
  'auteur'           => $jeu->getAuteur(),
  'editeur'          => $jeu->getEditeur(),
  'anneeEdition'     => $jeu->getAnneeEdition(),
  'dureePartie'      => $jeu->getDureePartie(),
  'type'             => $jeu->getType(),
  'complexite'       => $jeu->getComplexite(),
  'nbMin'            => $jeu->getNbMin(),
  'nbMax'            => $jeu->getNbMax(),
  'description'      => $jeu->getDescription(),


  'posseder'         => Possedants($jeu),
  'emprunts'         => Emprunts($jeu),
  );
  return $reponse;
}

function Possedants($jeu){
  $possedants = [];
  foreach ($jeu->getUserPossedantJeu() as $possedant) {
    $possedants[] = array(
      'user_id'            => $possedant->getUser()->getId(),
      'user_pseudo'        => $possedant->getUser()->getPseudo(),

      'posseder_niveau'             => $possedant->getNiveau(),
      'posseder_tuteur'             => $possedant->getTuteur(),
      'posseder_nb_exemplaire'      => $possedant->getNbExemplaire(),
      'posseder_preferences'        => $possedant->getPreferences(),
      'posseder_annee_acquisition'  => $possedant->getAnneeAcquisition(),
      'posseder_etat'               => $possedant->getEtat()
    );
  }
  return $possedants;
}

function Emprunts($jeu){
  $emprunts = [];
  foreach ($jeu->getListeEmprunt() as $emprunt) {
    $sessions = [];
    foreach ($emprunt->getSessions() as $session) {
      $sessions[] = array(
        'session_titre' => $session->getTitre(),
        'session_description' => $session->getDescription()
      );
    }
    $emprunts[] = array(
      'emprunt_dateEmprunt' => $emprunt->getDateEmprunt(),
      'emprunt_dateRetour' => $emprunt->getDateRetour(),
      'sessions' => $sessions
    );
  }
  return $emprunts;
}

/**
 * @Route("/PAL/api/v1.0")
 */
class JeuController extends AbstractController
{

  /**
   * Permet d'avoir la liste de tous les jeux
   * @Route("/jeu", name="liste_jeu", methods={"GET"})
   */
    public function listeJeu()
    {
      $repository   = $this->getDoctrine()->getRepository(Jeu::class);
      $listeJeu     = $repository->findAll();

      $listeReponse = array();
      foreach($listeJeu as $jeu){

        $listeReponse[] = jeuReponseBuilder($jeu);
        }
      $reponse = new Response();
      $reponse->setContent(json_encode(array("jeu" => $listeReponse)));
      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
}

/**
 * Permet de supprimer un jeu grâce à son id
 * @Route("/jeu", name="suppression_jeu", methods={"DELETE"})
 */
public function suppressionJeu(Request $request)
{
    $entityManager = $this->getDoctrine()->getManager();
    $repository    = $this->getDoctrine()->getRepository(Jeu::class);
    $body          = json_decode($request->getContent(), true);
    $id            = $body['id'];
    $jeu           = $repository->find($id);

    $entityManager->remove($jeu);
      $entityManager->flush();

    $reponse = new Response(json_encode(
      jeuReponseBuilder($jeu)
    ));

    $reponse->headers->set("Content-Type", "application/json");
    $reponse->headers->set("Access-Control-Allow-Origin", "*");
    return $reponse;
}

/**
 * Permet d'avoir les détails d'un jeu grâce à son id
 * @Route("/jeu/{id}", name="details_jeu", methods={"GET"})
 */
public function detailsJeu($id)
{
  $repository = $this->getDoctrine()->getRepository(Jeu::class);
  $jeu        = $repository->find($id);

  $reponse = new Response(json_encode(
    jeuReponseBuilder($jeu)
  ));

  $reponse->headers->set("Content-Type", "application/json");
  $reponse->headers->set("Access-Control-Allow-Origin", "*");
  return $reponse;
}



/**
 * Permet d'avoir les détails des jeu grâce à leur titre identique
 * @Route("/jeu/details/{titre}", name="details_jeu_by_titre", methods={"GET"})
 */
public function detailsJeuxByTitre($titre)
{
    $repository = $this->getDoctrine()->getRepository(Jeu::class);
    $jeux        = $repository->findByTitre($titre);

    $listeReponse = [];
    foreach ($jeux as $jeu) {
      $listeReponse[] = jeuReponseBuilder($jeu);
    }

    $reponse = new Response();
    $reponse->setContent(json_encode(array("jeu" => $listeReponse)));
    $reponse->headers->set("Content-Type", "application/json");
    $reponse->headers->set("Access-Control-Allow-Origin", "*");
    return $reponse;
}

    /**
    * Permet de créer un jeu
    * @Route("/jeu", name="nouveau_jeu", methods={"POST"})
    */
    public function nouveauJeu(Request $request)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $jeu           = new Jeu();
      $body          = json_decode($request->getContent(), true);

      $titre             = $body['titre'];
      $auteur            = $body['auteur'];
      $editeur           = $body['editeur'];
      $anneeEdition      = $body['anneeEdition'];
      $dureePartie       = $body['dureePartie'];
      $type              = $body['type'];
      $complexite        = $body['complexite'];
      $nbMin             = $body['nbMin'];
      $nbMax             = $body['nbMax'];
      $description       = $body['description'];


      $jeu->setTitre($titre);
      $jeu->setAuteur($auteur);
      $jeu->setEditeur($editeur);
      $jeu->setAnneeEdition($anneeEdition);
      $jeu->setDureePartie($dureePartie);
      $jeu->setType($type);
      $jeu->setComplexite($complexite);
      $jeu->setNbMin($nbMin);
      $jeu->setNbMax($nbMax);
      $jeu->setDescription($description);


      $entityManager->persist($jeu);
        $entityManager->flush();

      $reponse = new Response(json_encode(
        jeuReponseBuilder($jeu)
      ));

      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }


    /**
     * Permet de modifier les informations d'un jeu grâce à son id
     * La gestion des users du jeu se fera via l'entité User
     * La gestion des sessions du jeu se fera via l'entité Session
     * @Route("/jeu/{id}", name="modification_jeu", methods={"PUT"})
     */
    public function modificationJeu(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Jeu::class);
        $body   = json_decode($request->getContent(), true);
        $titre             = $body['titre'];
        $auteur            = $body['auteur'];
        $editeur           = $body['editeur'];
        $anneeEdition      = $body['anneeEdition'];
        $dureePartie       = $body['dureePartie'];
        $type              = $body['type'];
        $complexite        = $body['complexite'];
        $nbMin             = $body['nbMin'];
        $nbMax             = $body['nbMax'];
        $description       = $body['description'];


        $jeu = $repository->find($id);


        $jeu->setTitre($titre);
        $jeu->setAuteur($auteur);
        $jeu->setEditeur($editeur);
        $jeu->setAnneeEdition($anneeEdition);
        $jeu->setDureePartie($dureePartie);
        $jeu->setType($type);
        $jeu->setComplexite($complexite);
        $jeu->setNbMin($nbMin);
        $jeu->setNbMax($nbMax);
        $jeu->setDescription($description);


        $entityManager->persist($jeu);
          $entityManager->flush();
          $reponse = new Response(json_encode(
            jeuReponseBuilder($jeu)
          ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

}
