<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Session;
use App\Entity\Emprunter;
use App\Entity\User;

function sessionReponseBuilder($session){
  $reponse = array(
    'id'               => $session->getId(),
    'titre'            => $session->getTitre(),
    'description'      => $session->getDescription(),
    'nb_min'           => $session->getNbMin(),
    'nb_max'           => $session->getNbMax(),
    'duree'            => $session->getDuree(),
    'lieu'             => $session->getLieu(),
    'date'             => $session->getDate(),
    'heureDebut'       => $session->getHeuredebut(),
    'heureFin'         => $session->getHeurefin(),


    'emprunt_id'       => $session->getEmprunt()->getId(),
    'emprunt_user'     => $session->getEmprunt()->getUser()->getPseudo(),
    'jeu_id'           => $session->getEmprunt()->getJeu()->getId(),
    'jeu_titre'        => $session->getEmprunt()->getJeu()->getTitre(),
    'dateEmprunt'      => $session->getEmprunt()->getDateEmprunt(),
    'dateRetour'       => $session->getEmprunt()->getDateRetour(),
    'participants'     => Participants($session)
  );
  return $reponse;
}

//Fonction utilisées dans les méthode de la classe
function Participants($session){
  $participants = [];
  foreach ($session->getParticiper() as $participant) {
    $participants[] = array(
      'user_id' => $participant->getId(),
      'user_pseudo' => $participant->getPseudo(),
    );
  }
  return $participants;
}

/**
 * @Route("/PAL/api/v1.0")
 */
class SessionController extends AbstractController
{
  /**
   * Permet d'avoir la liste de tous les jeux
   * @Route("/session", name="liste_session", methods={"GET"})
   */
    public function listeSession()
    {
      $repository   = $this->getDoctrine()->getRepository(Session::class);
      $listeSession = $repository->findAll();
      $listeReponse = array();
      foreach($listeSession as $session){

        $listeReponse[] = sessionReponseBuilder($session);
      }
      $reponse = new Response();
      $reponse->setContent(json_encode(array("sessions" => $listeReponse)));
      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }

    /**
     * Permet d'avoir les détails d'un jeu grâce à son id
     * @Route("/session/{id}", name="details_session", methods={"GET"})
     */
    public function detailsSession($id)
    {
      $repository = $this->getDoctrine()->getRepository(Session::class);
      $session    = $repository->find($id);

      $reponse = new Response(json_encode(
        sessionReponseBuilder($session)
      ));

      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }

    /**
    * Permet de créer une nouvelle session
    * @Route("/session", name="nouvelle_session", methods={"POST"})
    */
    public function nouvelleSession(Request $request)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $session       = new Session();
      $body          = json_decode($request->getContent(), true);

      $emprunt_id    = $body['emprunt_id'];
      $titre         = $body['titre'];
      $description   = $body['description'];
      $nbMin         = $body['nb_min'];
      $nbMax         = $body['nb_max'];
      $duree         = $body['duree'];
      $lieu          = $body['lieu'];
      $date          = $body['date'];
      $heureDebut    = $body['heureDebut'];
      $heureFin      = $body['heureFin'];


      $session->setEmprunt(
        $this->getDoctrine()->getRepository(Emprunter::class)->find($emprunt_id)
      );
      $session->setTitre($titre);
      $session->setDescription($description);
      $session->setNbMin($nbMin);
      $session->setNbMax($nbMax);
      $session->setDuree($duree);
      $session->setLieu($lieu);
      $session->setDate(new \DateTime($date));
      // Concaténation de la date donnée avec l'heure pour les champs
      // Heuredebut et heurefin
      $session->setHeuredebut(new \DateTime($heureDebut));
      $session->setHeurefin(new \DateTime($heureFin));


      $entityManager->persist($session);
        $entityManager->flush();

      $reponse = new Response(json_encode(
        sessionReponseBuilder($session)
      ));

      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }

    /**
     * Permet de supprimer une session grâce à son id
     * @Route("/session", name="suppression_session", methods={"DELETE"})
     */
    public function suppressionSession(Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Session::class);
        $body          = json_decode($request->getContent(), true);
        $id            = $body['id'];
        $session       = $repository->find($id);
        $entityManager->remove($session);
          $entityManager->flush();

        $emprunt = $session->getEmprunt();

        $jeu_id      = $emprunt->getJeu()->getId();
        $jeu_titre   = $emprunt->getJeu()->getTitre();
        $DateEmprunt = $emprunt->getDateEmprunt();
        $DateRetour  = $emprunt->getDateRetour();

        $reponse = new Response(json_encode(
          sessionReponseBuilder($session)
        ));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }


    /**
     * fonction de modification d'une session
     * @Route("/session/{id}", name="modification_session", methods={"PUT"})
     */
    public function modificationSession(Request $request, $id)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $repository    = $this->getDoctrine()->getRepository(Session::class);
      $body          = json_decode($request->getContent(), true);

      $emprunt_id    = $body['emprunt_id'];
      $titre         = $body['titre'];
      $description   = $body['description'];
      $nbMin         = $body['nb_min'];
      $nbMax         = $body['nb_max'];
      $duree         = $body['duree'];
      $lieu          = $body['lieu'];
      $date          = $body['date'];
      $heureDebut    = $body['heureDebut'];
      $heureFin      = $body['heureFin'];

      $session = $repository->find($id);

      $session->setEmprunt(
        $this->getDoctrine()->getRepository(Emprunter::class)->find($emprunt_id)
      );
      $session->setDescription($description);
      $session->setTitre($titre);
      $session->setNbMin($nbMin);
      $session->setNbMax($nbMax);
      $session->setDuree($duree);
      $session->setLieu($lieu);
      $session->setDate(new \DateTime($date));
      $session->setHeuredebut(new \DateTime($date.$heureDebut));
      $session->setHeurefin(new \DateTime($date.$heureFin));

      $entityManager->persist($session);
        $entityManager->flush();

      $reponse = new Response(json_encode(
        sessionReponseBuilder($session)
      ));

      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }

    /**
     * fonction de modification d'une session
     * @Route("/session/addUser/{id}", name="ajout_participant", methods={"PUT"})
     */
    public function sessionAddUser(Request $request, $id){
      $entityManager = $this->getDoctrine()->getManager();
      $repository    = $this->getDoctrine()->getRepository(Session::class);
      $body          = json_decode($request->getContent(), true);

      $user_id       = $body['user_id'];

      $session = $repository->find($id);

      $session->addParticiper(
        $this->getDoctrine()->getRepository(User::class)->find($user_id)
      );

      $entityManager->persist($session);
        $entityManager->flush();

        $reponse = new Response(json_encode(
          sessionReponseBuilder($session)
        ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }


    /**
     * fonction de modification d'une session
     * @Route("/session/removeUser/{id}", name="suppression_participant", methods={"PUT"})
     */
    public function sessionRemoveUser(Request $request, $id){
      $entityManager = $this->getDoctrine()->getManager();
      $repository    = $this->getDoctrine()->getRepository(Session::class);
      $body          = json_decode($request->getContent(), true);

      $user_id       = $body['user_id'];

      $session = $repository->find($id);

      $session->removeParticiper(
        $this->getDoctrine()->getRepository(User::class)->find($user_id)
      );

      $entityManager->persist($session);
        $entityManager->flush();

        $reponse = new Response(json_encode(
          sessionReponseBuilder($session)
        ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

}
