<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Entity\Session;

function userReponseBuilder($user){
  $reponse = array(
    'id'           => $user->getId(),
    'pseudo'       => $user->getPseudo(),
    'email'        => $user->getEmail(),
    'lieu'         => $user->getLieu(),
    'description'  => $user->getDescription(),
    'privileges'   => $user->getPrivileges(),

    'listeJeux'    => Jeux($user),
    'listeGroup'   => Group($user),
    'listeSession' => Session($user),
    'listeEmprunt' => Emprunt($user),
  );
  return $reponse;
}

function Jeux($user){
  $jeux = [];
    foreach($user->getJeuxPossedes() as $possession){
        $jeu = $possession->getJeu();
        $jeux[] = array(
          'jeu_id'            => $jeu->getId(),
          'jeu_titre'         => $jeu->getTitre(),
          'jeu_niveau'        => $possession->getNiveau(),
          'jeu_tuteur'        => $possession->getTuteur(),
          'jeu_nb_exemplaire' => $possession->getNbExemplaire(),
          'jeu_preferences'   => $possession->getPreferences(),
          'jeu_anneeAcquisition' => $possession->getAnneeAcquisition(),
          'jeu_getEtat'      => $possession->getEtat()
         );
       }
  return $jeux;
}

function Group($user){
  $groups = [];
  foreach ($user->getInclures() as $inclure) {
    $group = $inclure->getGrup();
    $groups[] = array(
      'group_id'             => $group->getId(),
      'group_titre'          => $group->getTitre(),
      'group_description'    => $group->getDescription(),
      'group_public'         => $group->getPublic()
    );
  }
  return $groups;
}

function Session($user){
  $sessions = [];
  foreach ($user->getSessions() as $session) {
    $sessions[] = array(
      'session_id'             => $session->getId(),
      'session_titre'          => $session->getTitre(),
      'session_titre_jeu'      => $session->getEmprunt()->getJeu()->getTitre(),
      'session_user'           => $session->getEmprunt()->getUser()->getPseudo(),
      'session_description'    => $session->getDescription(),
      'session_nb_min'         => $session->getNbMin(),
      'session_nb_max'         => $session->getNbMax(),
      'session_duree'          => $session->getDuree(),
      'session_lieu'           => $session->getLieu(),
      'session_date'           => $session->getDate(),
      'session_participer'     => $session->getParticiper(),
      'session_heuredebut'     => $session->getHeuredebut(),
      'session_heurefin'       => $session->getHeurefin(),
    );
  }
  return $sessions;
}

function Emprunt($user){
  $emprunters = [];
  foreach ($user->getEmprunters() as $emprunter) {
    $emprunters[] = array (
    'emprunt_id' => $emprunter->getId(),
    'emprunt_dateEmprunt' => $emprunter->getDateEmprunt(),
    'emprunt_dateRetour' => $emprunter->getDateRetour()
    );
  }
  return $emprunters;
}

/**
 * @Route("/PAL/api/v1.0")
 */
class UserController extends AbstractController
{
  /**
   * Permet d'avoir la liste de tous les users
   * @Route("/user", name="liste_user", methods={"GET"})
   */
    public function listeUser()
    {
      $repository = $this->getDoctrine()->getRepository(User::class);
      $listeUser = $repository->findAll();
      $listeReponse = array();

      foreach($listeUser as $user){

        $listeReponse[] = userReponseBuilder($user);
      }
      $reponse = new Response();
      $reponse->setContent(json_encode(array("user" => $listeReponse)));
      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }
    /**
     * Permet d'avoir les détails d'un user
     * @Route("/user/{id}", name="details_user", methods={"GET"})
     */
     public function detailsUser($id)
     {
         $repository = $this->getDoctrine()->getRepository(User::class);
         $user = $repository->find($id);

         $reponse = new Response(json_encode(
           userReponseBuilder($user)
         ));

         $reponse->headers->set("Content-Type", "application/json");
         $reponse->headers->set("Access-Control-Allow-Origin", "*");
         return $reponse;
     }

     /**
      * Permet d'avoir les détails d'un user
      * @Route("/user/details/{pseudo}", name="details_user_by_pseudo", methods={"GET"})
      */
      public function detailsUserByPseudo($pseudo)
      {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->findOneByPseudo($pseudo);

         $reponse = new Response(json_encode(
           userReponseBuilder($user)
         ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
      }


     /**
     * Permet de créer un user
     * @Route("/user", name="nouveau_user", methods={"POST"})
     */
     public function nouveauUser(Request $request)
     {
       $entityManager = $this->getDoctrine()->getManager();
       $user          = new User();
       $body          = json_decode($request->getContent(), true);

       $pseudo      = $body['pseudo'];
       $email       = $body['email'];
       $lieu        = $body['lieu'];
       $description = $body['description'];
       $privileges  = $body['privileges'];

       $user->setPseudo($pseudo);
       $user->setEmail($email);
       $user->setLieu($lieu);
       $user->setDescription($description);
       $user->setPrivileges($privileges);
       $entityManager->persist($user);
         $entityManager->flush();

       $reponse = new Response(json_encode(
         userReponseBuilder($user)
       ));

       $reponse->headers->set("Content-Type", "application/json");
       $reponse->headers->set("Access-Control-Allow-Origin", "*");
       return $reponse;
     }
     /**
    * Permet de modifier les informations d'un user grâce à son id
    * La gestion des groupes du user se fera via l'entité Group
    * La gestion des jeux du user se fera via l'entité Jeu
    * La gestion des sessions du user se fera via l'entité Session
    * @Route("/user/{id}", name="modification_user", methods={"PUT"})
    */
   public function modificationUser(Request $request, $id)
   {
     $entityManager = $this->getDoctrine()->getManager();
     $repository    = $this->getDoctrine()->getRepository(User::class);
     $body          = json_decode($request->getContent(), true);

     $pseudo      = $body['pseudo'];
     $email       = $body['email'];
     $lieu        = $body['lieu'];
     $description = $body['description'];
     $privileges  = $body['privileges'];

     $user        = $repository->find($id);

     $user->setPseudo($pseudo);
     $user->setEmail($email);
     $user->setLieu($lieu);
     $user->setDescription($description);
     $user->setPrivileges($privileges);
     $entityManager->persist($user);
       $entityManager->flush();
       $reponse = new Response(json_encode(
         userReponseBuilder($user)
       ));

     $reponse->headers->set("Content-Type", "application/json");
     $reponse->headers->set("Access-Control-Allow-Origin", "*");
     return $reponse;
   }
     /**
    * Permet de supprimer un user grâce à son id
    * @Route("/user", name="suppression_user", methods={"DELETE"})
    */
   public function suppressionUser(Request $request)
   {
     $entityManager = $this->getDoctrine()->getManager();
     $repository    = $this->getDoctrine()->getRepository(User::class);
     $body          = json_decode($request->getContent(), true);
     $id            = $body['id'];
     $user          = $repository->find($id);
     $entityManager->remove($user);
       $entityManager->flush();

     $reponse = new Response(json_encode(
       userReponseBuilder($user)
     ));

     $reponse->headers->set("Content-Type", "application/json");
     $reponse->headers->set("Access-Control-Allow-Origin", "*");
     return $reponse;
  }

  /**
  * Route pour ajouter une session auxquel participe l'utilisateur
  *@Route("/user/addSession/{id}", name="ajout_session", methods="PUT")
  */
  public function userAddSession(Request $request, $id)
  {
    $entityManager = $this->getDoctrine()->getManager();
    $repository    = $this->getDoctrine()->getRepository(User::class);
    $body          = json_decode($request->getContent(), true);
    $session_id            = $body['session_id'];

    $user = $repository->find($id);

    $user->addSession(
      $this->getDoctrine()->getRepository(Session::class)->find($session_id)
    );

    $entityManager->persist($user);
      $entityManager->flush();

    $reponse = new Response(json_encode(
      userReponseBuilder($user)
    ));
     $reponse->headers->set("Content-Type", "application/json");
     $reponse->headers->set("Access-Control-Allow-Origin", "*");
     return $reponse;
  }

  /**
  * Route pour ajouter une session auxquel participe l'utilisateur
  *@Route("/user/removeSession/{id}", name="remove_session", methods="PUT")
  */
  public function userRemoveSession(Request $request, $id)
  {
    $entityManager = $this->getDoctrine()->getManager();
    $repository    = $this->getDoctrine()->getRepository(User::class);
    $body          = json_decode($request->getContent(), true);
    $session_id            = $body['session_id'];

    $user = $repository->find($id);

    $user->removeSession(
      $this->getDoctrine()->getRepository(Session::class)->find($session_id)
    );

    $entityManager->persist($user);
      $entityManager->flush();

    $reponse = new Response(json_encode(
      userReponseBuilder($user)
    ));
     $reponse->headers->set("Content-Type", "application/json");
     $reponse->headers->set("Access-Control-Allow-Origin", "*");
     return $reponse;
  }
}
