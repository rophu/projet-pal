<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Group;

function groupReponseBuilder($group){
  $reponse = array (
  'id'          => $group->getId(),
  'titre'       => $group->getTitre(),
  'description' => $group->getDescription(),
  'public'      => $group->getPublic(),

  'userInclus'  => UserInclus($group)
  );
  return $reponse;
}

// Extrait les propriétés d'inclure et l'id et le pseudo de l'utilisateur
function UserInclus($group){
  $userInclus = [];
  foreach ($group->getInclures() as $inclu) {
    $user = $inclu->getUser();
    $userInclus[] = array(
      'user_id'     => $user->getId(),
      'user_pseudo' => $user->getPseudo(),
      'responsable' => $inclu->getResponsable()
    );
    //Fin foreach des relations inclures du group
  }
  return $userInclus;
}


/**
 * @Route("/PAL/api/v1.0")
 */
class GroupController extends AbstractController
{
      /**
       * Permet d'avoir la liste de tous les groupes
       * @Route("/group", name="liste_group", methods={"GET"})
       */
        public function listeGroup()
        {
          $repository = $this->getDoctrine()->getRepository(Group::class);
          $listeGroup = $repository->findAll();
          $listeReponse = array();
          foreach($listeGroup as $group){

            //$userInclus = UserInclus($group);

            $listeReponse[] = groupReponseBuilder($group);
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("group" => $listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
      }

      /**
       * Permet d'avoir les détails d'un group via l'id
       * @Route("/group/{id}", name="details_group_by_id", methods={"GET"})
       */
       public function detailsGroup($id)
       {
         $repository = $this->getDoctrine()->getRepository(Group::class);
         $group = $repository->find($id);

         $reponse = new Response(json_encode(
           groupReponseBuilder($group)
         ));

         $reponse->headers->set("Content-Type", "application/json");
         $reponse->headers->set("Access-Control-Allow-Origin", "*");
         return $reponse;
     }

    /**
     * Permet d'avoir les détails d'un group à l'aide de son titre
     * @Route("/group/details/{titre}", name="details_group_by_titre", methods={"GET"})
     */
     public function detailsGroupByTitre($titre)
     {
       $repository = $this->getDoctrine()->getRepository(Group::class);
       $group = $repository->findOneTitre($titre);

       $reponse = new Response(json_encode(
         groupReponseBuilder($group)
       ));

       $reponse->headers->set("Content-Type", "application/json");
       $reponse->headers->set("Access-Control-Allow-Origin", "*");
       return $reponse;
     }

     /**
     * Permet de créer un groupe
     * @Route("/group", name="nouveau_group", methods={"POST"})
     */
     public function nouveauGroup(Request $request)
     {
       $entityManager = $this->getDoctrine()->getManager();
       $group   = new Group();
       $body    = json_decode($request->getContent(), true);
       $titre   = $body['titre'];
       $description    = $body['description'];
       $public  = $body['public'];

       $group->setTitre($titre);
       $group->setDescription($description);
       $group->setPublic($public);
       $entityManager->persist($group);
         $entityManager->flush();

       $reponse = new Response(json_encode(
         groupReponseBuilder($group)
       ));

       $reponse->headers->set("Content-Type", "application/json");
       $reponse->headers->set("Access-Control-Allow-Origin", "*");
       return $reponse;
     }

     /**
     * Permet de modifier les informations d'un group grâce à son id
     * @Route("/group/{id}", name="modification_group", methods={"PUT"})
     */
     public function modificationGroup(Request $request, $id)
     {
       $entityManager = $this->getDoctrine()->getManager();
       $repository    = $this->getDoctrine()->getRepository(Group::class);
       $body = json_decode($request->getContent(), true);

       $titre         = $body['titre'];
       $description   = $body['description'];
       $public        = $body['public'];
       $group         = $repository->find($id);

       $group->setTitre($titre);
       $group->setDescription($description);
       $group->setPublic($public);
       $entityManager->persist($group);
         $entityManager->flush();

       $reponse = new Response(json_encode(
         groupReponseBuilder($group)
       ));

       $reponse->headers->set("Content-Type", "application/json");
       $reponse->headers->set("Access-Control-Allow-Origin", "*");
       return $reponse;
     }
     /**
     * Permet de supprimer un group grâce à son id
     * @Route("/group", name="suppression_group", methods={"DELETE"})
     */
     public function suppressionGroup(Request $request)
     {
       $entityManager = $this->getDoctrine()->getManager();
       $repository    = $this->getDoctrine()->getRepository(Group::class);
       $body          = json_decode($request->getContent(), true);
       $id            = $body['id'];
       $group         = $repository->find($id);
       $entityManager->remove($group);
         $entityManager->flush();

       $reponse = new Response(json_encode(
         groupReponseBuilder($group)
         ));
       $reponse->headers->set("Content-Type", "application/json");
       $reponse->headers->set("Access-Control-Allow-Origin", "*");
       return $reponse;
    }
}
