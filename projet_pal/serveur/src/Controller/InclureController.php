<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Inclure;
use App\Entity\Group;
use App\Entity\User;

function inclureReponseBuilder($inclure){
  return array(
    'id'               => $inclure->getId(),
    'responsable'       => $inclure->getResponsable(),
    'pseudo'           => $inclure->getUser()->getPseudo(),
    'groupeTitre'      => $inclure->getGrup()->getTitre(),
  );
}

/**
 * @Route("/PAL/api/v1.0")
 */
class InclureController extends AbstractController
{
  /**
   * Permet d'avoir la liste de tous les jeux
   * @Route("/inclure", name="liste_inclure", methods={"GET"})
   */
    public function listeInclure()
    {
      $repository = $this->getDoctrine()->getRepository(Inclure::class);
      $listeInclure = $repository->findAll();
      $listeReponse = array();
      foreach($listeInclure as $inclure){

          $listeReponse[] = inclureReponseBuilder($inclure);
    }
    $reponse = new Response();
    $reponse->setContent(json_encode(array("inclure" => $listeReponse)));
    $reponse->headers->set("Content-Type", "application/json");
    $reponse->headers->set("Access-Control-Allow-Origin", "*");
    return $reponse;
}

/**
 * Permet d'avoir les détails d'un jeu grâce à son id
 * @Route("/inclure/{id}", name="details_inclure", methods={"GET"})
 */
public function detailsInclure($id)
{
    $repository = $this->getDoctrine()->getRepository(Inclure::class);
    $inclure        = $repository->find($id);

    $reponse = new Response(json_encode(
      inclureReponseBuilder($inclure)
    ));


    $reponse->headers->set("Content-Type", "application/json");
    $reponse->headers->set("Access-Control-Allow-Origin", "*");
    return $reponse;
}

    /**
    * Permet de créer un jeu
    * @Route("/inclure", name="nouveau_inclure", methods={"POST"})
    */
    public function nouveauInclure(Request $request)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $inclure  = new Inclure();
      $body = json_decode($request->getContent(), true);
      $reponsable             = $body['responsable'];
      $user_id             = $body['user_id'];
      $group_id            = $body['group_id'];

      $inclure->setResponsable($reponsable);
      $inclure->setUser(
        $this->getDoctrine()->getRepository(User::class)->find($user_id)
      );
      $inclure->setGrup(
        $this->getDoctrine()->getRepository(Group::class)->find($group_id)
      );

      $entityManager->persist($inclure);
      $entityManager->flush();
      $reponse = new Response(json_encode(
        inclureReponseBuilder($inclure)
      ));

      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }

    /**
     * Permet de supprimer un jeu grâce à son id
     * @Route("/inclure", name="suppression_inclure", methods={"DELETE"})
     */
    public function suppressionInclure(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Inclure::class);
        $body          = json_decode($request->getContent(), true);
        $id            = $body['id'];
        $inclure       = $repository->find($id);

        $entityManager->remove($inclure);
        $entityManager->flush();

        $reponse = new Response(json_encode(
          inclureReponseBuilder($inclure)
        ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * Permet de modifier les informations d'un jeu grâce à son id
     * La gestion des users du jeu se fera via l'entité User
     * La gestion des sessions du jeu se fera via l'entité Session
     * @Route("/inclure/{id}", name="modification_inclure", methods={"PUT"})
     */
    public function modificationInclure(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Inclure::class);
        $body   = json_decode($request->getContent(), true);
        $responsable      = $body['responsable'];
        $user_id          = $body['user_id'];
        $group_id         = $body['group_id'];

        $inclure          = $repository->find($id);
        $inclure->setResponsable($responsable);
        $inclure->setUser(
          $this->getDoctrine()->getRepository(User::class)->find($user_id)
        );
        $inclure->setGrup(
          $this->getDoctrine()->getRepository(Group::class)->find($group_id)
        );

        $entityManager->persist($inclure);
          $entityManager->flush();
          $reponse = new Response(json_encode(
            inclureReponseBuilder($inclure)
          ));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

}
