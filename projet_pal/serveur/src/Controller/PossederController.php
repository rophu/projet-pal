<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Posseder;
use App\Entity\Jeu;
use App\Entity\User;

function possedeReponseBuilder($possession){
  return array(
    'id'               => $possession->getId(),
    'niveau'           => $possession->getNiveau(),
    'tuteur'           => $possession->getTuteur(),
    'nb_exemplaire'    => $possession->getNbExemplaire(),
    'preferences'      => $possession->getPreferences(),
    'anneeAcquisition' => $possession->getAnneeAcquisition(),
    'etat'             => $possession->getEtat(),

    'proprietaire'    => Proprietaire($possession),
    'jeu'             => JeuPossede($possession),
  );
}

function JeuPossede($possession){
  return array(
    'jeu_id'               => $possession->getJeu()->getId(),
    'jeu_titre'            => $possession->getJeu()->getTitre(),
    'jeu_auteur'           => $possession->getJeu()->getAuteur(),
    'jeu_editeur'          => $possession->getJeu()->getEditeur(),
    'jeu_anneeEdition'     => $possession->getJeu()->getAnneeEdition(),
    'jeu_dureePartie'      => $possession->getJeu()->getDureePartie(),
    'jeu_type'             => $possession->getJeu()->getType(),
    'jeu_complexite'       => $possession->getJeu()->getComplexite(),
    'jeu_nbMin'            => $possession->getJeu()->getNbMin(),
    'jeu_nbMax'            => $possession->getJeu()->getNbMax(),
    'jeu_description'      => $possession->getJeu()->getDescription()
  );
}

function Proprietaire($possession){
  return array(
    'propriétaire_id'               => $possession->getUser()->getId(),
    'proprietaire_pseudo'           => $possession->getUser()->getPseudo(),
    'proprietaire_email'            => $possession->getUser()->getEmail(),
    'proprietaire_lieu'             => $possession->getUser()->getLieu(),
    'proprietaire_descriptionUser'  => $possession->getUser()->getDescription(),
    'proprietaire_privileges'       => $possession->getUser()->getPrivileges(),
  );
}

/**
 * @Route("/PAL/api/v1.0")
 */
class PossederController extends AbstractController
{
  /**
   * Permet d'avoir la liste de tous les jeux
   * @Route("/posseder", name="liste_posseder", methods={"GET"})
   */
    public function listePosseder()
    {
      $repository = $this->getDoctrine()->getRepository(Posseder::class);
      $listePossession = $repository->findAll();
      $listeReponse = array();
      foreach($listePossession as $possession){

          $listeReponse[] = possedeReponseBuilder($possession);
        }
      $reponse = new Response();
      $reponse->setContent(json_encode(array("possesions" => $listeReponse)));
      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
}

/**
 * Permet d'avoir les détails d'un jeu grâce à son id
 * @Route("/posseder/{id}", name="details_posseder", methods={"GET"})
 */
public function detailsPossede($id)
{
    $repository      = $this->getDoctrine()->getRepository(Posseder::class);
    $possession = $repository->find($id);

    $reponse = new Response(json_encode(
      possedeReponseBuilder($possession)
    ));
    $reponse->headers->set("Content-Type", "application/json");
    $reponse->headers->set("Access-Control-Allow-Origin", "*");
    return $reponse;
}

    /**
    * Permet de créer un jeu
    * @Route("/posseder", name="nouveau_posseder", methods={"POST"})
    */
    public function nouveauPosseder(Request $request)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $possede  = new Posseder();
      $body = json_decode($request->getContent(), true);
      $user_id           = $body['user_id'];
      $jeu_id           = $body['jeu_id'];
      $niveau            = $body['niveau'];
      $tuteur            = $body['tuteur'];
      $nb_exemplaire     = $body['nb_exemplaire'];
      $preferences       = $body['preferences'];
      $AnneeAcquisition  = $body['anneeAcquisition'];
      $etat              = $body['etat'];

      $possede->setUser(
        $this->getDoctrine()->getRepository(User::class)->find($user_id)
      );
      $possede->setJeu(
        $this->getDoctrine()->getRepository(Jeu::class)->find($jeu_id)
      );
      $possede->setNiveau($niveau);
      $possede->setTuteur($tuteur);
      $possede->setNbExemplaire($nb_exemplaire);
      $possede->setPreferences($preferences);
      $possede->setAnneeAcquisition($AnneeAcquisition);
      $possede->setEtat($etat);

      $entityManager->persist($possede);
      $entityManager->flush();

      $reponse = new Response(json_encode(
        possedeReponseBuilder($possede)
      ));

      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }

    /**
     * Permet de supprimer un jeu grâce à son id
     * @Route("/posseder", name="suppression_posseder", methods={"DELETE"})
     */
    public function suppressionPosseder(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Posseder::class);
        $body          = json_decode($request->getContent(), true);
        $id            = $body['id'];
        $possede           = $repository->find($id);
        $entityManager->remove($possede);
        $entityManager->flush();

        $reponse = new Response(json_encode(
          possedeReponseBuilder($possede)
        ));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
    /**
     * Permet de modifier les informations d'un jeu grâce à son id
     * La gestion des users du jeu se fera via l'entité User
     * La gestion des sessions du jeu se fera via l'entité Session
     * @Route("/posseder/{id}", name="modification_posseder", methods={"PUT"})
     */
    public function modificationPosseder(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Posseder::class);
        $body   = json_decode($request->getContent(), true);
        $user_id           = $body['user_id'];
        $jeu_id           = $body['jeu_id'];
        $niveau            = $body['niveau'];
        $tuteur            = $body['tuteur'];
        $nb_exemplaire     = $body['nb_exemplaire'];
        $preferences       = $body['preferences'];
        $AnneeAcquisition  = $body['anneeAcquisition'];
        $etat              = $body['etat'];

        $possede = $repository->find($id);

        $possede->setUser(
          $this->getDoctrine()->getRepository(User::class)->find($user_id)
        );
        $possede->setJeu(
          $this->getDoctrine()->getRepository(Jeu::class)->find($jeu_id)
        );
        $possede->setNiveau($niveau);
        $possede->setTuteur($tuteur);
        $possede->setNbExemplaire($nb_exemplaire);
        $possede->setPreferences($preferences);
        $possede->setAnneeAcquisition($AnneeAcquisition);
        $possede->setEtat($etat);

        $entityManager->persist($possede);
        $entityManager->flush();

        $reponse = new Response(json_encode(
          possedeReponseBuilder($possede)
        ));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

}
