<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Emprunter;
use App\Entity\Jeu;
use App\Entity\User;

function empruntReponseBuilder($emprunt){
  $reponse = array(
    'id' => $emprunt->getId(),
    'dateEmprunt' => $emprunt->getDateEmprunt(),
    'dateRetour'  => $emprunt->getDateRetour(),
    'jeu_id'      => $emprunt->getJeu()->getId(),
    'user_id'      => $emprunt->getUser()->getId(),

    'jeu_titre'            => $emprunt->getJeu()->getTitre(),
    'jeu_auteur'           => $emprunt->getJeu()->getAuteur(),
    'jeu_editeur'          => $emprunt->getJeu()->getEditeur(),
    'jeu_anneeEdition'     => $emprunt->getJeu()->getAnneeEdition(),
    'jeu_dureePartie'      => $emprunt->getJeu()->getDureePartie(),
    'jeu_type'             => $emprunt->getJeu()->getType(),
    'jeu_complexite'       => $emprunt->getJeu()->getComplexite(),
    'jeu_nbMin'            => $emprunt->getJeu()->getNbMin(),
    'jeu_nbMax'            => $emprunt->getJeu()->getNbMax(),
    'jeu_description'      => $emprunt->getJeu()->getDescription(),

    'sessions'    => Sessions($emprunt)
  );
  return $reponse;
}

function Sessions($emprunt){
  //Récupération des sessions utilisant ce jeu
  //(voir pour faire en sorte de ne récupérer que les sessions à venir)
  $sessions = [];
  foreach ($emprunt->getSessions() as $session) {
    $sessions = array(
      'session_id'          => $session->getId(),
      'session_titre'       => $session->getTitre(),
      'session_description' => $session->getDescription(),
    );
  }

  return $sessions;
}

/**
 * @Route("/PAL/api/v1.0")
 */
class EmprunterController extends AbstractController
{
    /**
     * Permet d'avoir la liste de tous les jeux
     * @Route("/emprunt", name="liste_emprunts", methods={"GET"})
     */
    public function listeEmprunt()
    {
      $repository   = $this->getDoctrine()->getRepository(Emprunter::class);
      $listeEmprunt = $repository->findAll();
      $listeReponse = array();

      foreach($listeEmprunt as $emprunt){

        $listeReponse[] = empruntReponseBuilder($emprunt);
      }

      $reponse = new Response();
      $reponse->setContent(json_encode(array("emprunts" => $listeReponse)));
      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }

/**
 * Permet d'avoir les détails d'un jeu grâce à son id
 * @Route("/emprunt/{id}", name="details_emprunt", methods={"GET"})
 */
public function detailsEmprunt($id)
{
    $repository = $this->getDoctrine()->getRepository(Emprunter::class);
    $emprunt        = $repository->find($id);

    //Récupération des sessions utilisant ce jeu
    //(voir pour faire en sorte de ne récupérer que les sessions à venir)
    $jeu = $emprunt->getJeu();

    $reponse = new Response(json_encode(
      empruntReponseBuilder($emprunt)
    ));
    $reponse->headers->set("Content-Type", "application/json");
    $reponse->headers->set("Access-Control-Allow-Origin", "*");
    return $reponse;
}

    /**
    * Permet de créer un jeu
    * @Route("/emprunt", name="nouveau_emprunt", methods={"POST"})
    */
    public function nouveauEmprunt(Request $request)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $emprunt  = new Emprunter();
      $body = json_decode($request->getContent(), true);
      $jeu_id            = $body['jeu_id'];
      $user_id            = $body['user_id'];
      $dateEmprunt       = $body['dateEmprunt'];
      $dateRetour        = $body['dateRetour'];

      $emprunt->setJeu(
        $this->getDoctrine()->getRepository(Jeu::class)->find($jeu_id)
      );
      $emprunt->setUser(
        $this->getDoctrine()->getRepository(User::class)->find($user_id)
      );
      $emprunt->setDateEmprunt(new \DateTime($dateEmprunt));
      $emprunt->setDateRetour(new \DateTime($dateRetour));

      $entityManager->persist($emprunt);
        $entityManager->flush();
        $reponse = new Response(json_encode(
          empruntReponseBuilder($emprunt)
        ));

      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }

    /**
     * Permet de supprimer un emprunt grâce à son id
     * @Route("/emprunt", name="suppression_emprunt", methods={"DELETE"})
     */
    public function suppressionEmprunt(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Emprunter::class);
        $body          = json_decode($request->getContent(), true);
        $id            = $body['id'];
        $emprunt       = $repository->find($id);
        $entityManager->remove($emprunt);
          $entityManager->flush();

        $reponse = new Response(json_encode(
            empruntReponseBuilder($emprunt)
        ));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
    /**
     * Permet de modifier les informations d'un jeu grâce à son id
     * La gestion des users du jeu se fera via l'entité User
     * La gestion des sessions du jeu se fera via l'entité Session
     * @Route("/emprunt/{id}", name="modification_emprunt", methods={"PUT"})
     */
    public function modificationEmprunt(Request $request, $id)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $repository    = $this->getDoctrine()->getRepository(Emprunter::class);
      $body          = json_decode($request->getContent(), true);

      $dateEmprunt           = $body['dateEmprunt'];
      $dateRetour            = $body['dateRetour'];
      $jeu_id                = $body['jeu_id'];
      $user_id                = $body['user_id'];


      $emprunt = $repository->find($id);

      $emprunt->setJeu(
        $this->getDoctrine()->getRepository(Jeu::class)->find($jeu_id)
      );
      $emprunt->setUser(
        $this->getDoctrine()->getRepository(User::class)->find($user_id)
      );
      $emprunt->setDateEmprunt(new \DateTime($dateEmprunt));
      $emprunt->setDateRetour(new \DateTime($dateRetour));

      $entityManager->persist($emprunt);
        $entityManager->flush();
        $reponse = new Response(json_encode(
            empruntReponseBuilder($emprunt)
        ));
      $reponse->headers->set("Content-Type", "application/json");
      $reponse->headers->set("Access-Control-Allow-Origin", "*");
      return $reponse;
    }

}
