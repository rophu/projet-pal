<?php

namespace App\DataFixtures;

use App\Entity\Emprunter;
use App\Entity\Group;
use App\Entity\Inclure;
use App\Entity\Jeu;
use App\Entity\Posseder;
use App\Entity\Session;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      $faker = Faker\Factory::create('en_US');
      $users = Array();
      for ($i = 0; $i < 20; $i++) {
          $users[$i] = new User();
          $users[$i]->setPseudo($faker->firstName($gender = 'male'|'female'));
          $users[$i]->setEmail($faker->email);
          $users[$i]->setLieu($faker->randomElement($array = array ('Orléans','Saint-Jean-Le-Blanc','Saran','Fleury-les-Aubraies')));
          $users[$i]->setDescription($faker->sentence($nbWords = 6, $variableNbWords = true));
          $users[$i]->setPrivileges($faker->randomElement($array = array ('Nain jaune','champion intercommunal de Scrabble','dieu des échecs','Imperium gamemaster')));

          $manager->persist($users[$i]);
        }
        $jeux = Array();
        for ($i = 0; $i < 50; $i++) {
          $jeux[$i] = new Jeu();
          $jeux[$i]->setTitre($faker->word);
          $jeux[$i]->setAuteur($faker->name($gender = 'male'|'female'));
          $jeux[$i]->setEditeur($faker->randomElement($array = array ('Edge','Asmodée','Ravensburger','DaVinci games','Guillotine games')));
          $jeux[$i]->setAnneeEdition($faker->numberBetween($min = 1998, $max = 2017));
          $jeux[$i]->setDureePartie($faker->randomElement($array = array (30,60,90,120,150,180,240)));
          $jeux[$i]->setType($faker->randomElement($array = array ('Stratégie','coopératif','enquête','gestion')));
          $jeux[$i]->setComplexite($faker->randomDigit);
          $jeux[$i]->setNbMin($faker->numberBetween($min = 1, $max = 2));
          $jeux[$i]->setNbMax($faker->numberBetween($min = 3, $max = 10));
          $jeux[$i]->setDescription($faker->sentence($nbWords = 10, $variableNbWords = true));

          $manager->persist($jeux[$i]);
      }

      $groupes = Array();
      for ($i = 0; $i < 5; $i++) {
        $groupes[$i] = new Group();
        $groupes[$i]->setTitre($faker->word);
        $groupes[$i]->setDescription($faker->sentence($nbWords = 10, $variableNbWords = true));
        $groupes[$i]->setPublic($faker->boolean);

        $manager->persist($groupes[$i]);
      }
        $inclures = Array();
        for ($i = 0; $i < 5; $i++) {
          $inclures[$i] = new Inclure();
          $inclures[$i]->setResponsable($faker->boolean);
          $inclures[$i]->setGrup($groupes[$i]);
          $inclures[$i]->setUser($users[$i%5]);

          $manager->persist($inclures[$i]);
        }
        $jeuxPossedes = Array();
        for ($i = 0; $i < 50; $i++) {
            $jeuxPossedes[$i] = new Posseder();
            $jeuxPossedes[$i]->setNiveau($faker->randomDigit);
            $jeuxPossedes[$i]->setTuteur($faker->numberBetween($min = 0, $max = 1));
            $jeuxPossedes[$i]->setNbExemplaire($faker->numberBetween($min = 0, $max = 2));
            $jeuxPossedes[$i]->setPreferences($faker->randomElement($array = array ('un peu','beaucoup','passionément','à la folie')));
            $jeuxPossedes[$i]->setAnneeAcquisition($faker->numberBetween($min = 2010, $max = 2020));
            $jeuxPossedes[$i]->setEtat($faker->randomElement($array = array ('Bon','excellent','déplorable','presque pas abimé')));
            $jeuxPossedes[$i]->setUser($users[$i % 4]);
            $jeuxPossedes[$i]->setJeu($jeux[$i % 2]);

            $manager->persist($jeuxPossedes[$i]);
          }
          $emprunts = Array();
          for ($i = 0; $i < 10; $i++) {
              $emprunts[$i] = new Emprunter();
              $emprunts[$i]->setDateEmprunt($faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null));
              $emprunts[$i]->setDateRetour($faker->dateTimeBetween($startDate = '+1 years', $endDate = 'now + 1 years', $timezone = null));
              //$emprunts[$i]->setSession($sessions[$i]);
              $emprunts[$i]->setJeu($jeux[$i % 3]);
              $emprunts[$i]->setUser($users[$i % 3]);

              $manager->persist($emprunts[$i]);
            }

            $sessions = Array();
            for ($i = 0; $i < 10; $i++) {
              $sessions[$i] = new Session();
              $sessions[$i]->setTitre($faker->sentence($nbWords = 3, $variableNbWords = true));
              $sessions[$i]->setDescription($faker->sentence($nbWords = 35, $variableNbWords = true));
              $sessions[$i]->setNbMin($faker->numberBetween($min = 1, $max = 2));
              $sessions[$i]->setNbMax($faker->numberBetween($min = 3, $max = 10));
              $sessions[$i]->setDuree($faker->randomElement($array = array (30,60,90,120,150,180,240)));
              $sessions[$i]->setLieu($faker->randomElement($array = array ('salle du temps perdu 39 rue des étalons Orléans','29 rue des acacias Orléans','191 rue des murets Saran')));
              $sessions[$i]->setDate($faker->dateTime($min = 'now', $timezone = null));
              $sessions[$i]->setHeuredebut($faker->dateTime($min = 'now', $timezone = null));
              $sessions[$i]->setHeurefin($faker->dateTime($min = 'now', $timezone = null));
              $sessions[$i]->addParticiper($users[$i%3]);
              $sessions[$i]->setEmprunt($emprunts[$i%5]);

              $manager->persist($sessions[$i]);
            }


        $manager->flush();
    }
}
