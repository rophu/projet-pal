<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200531192509 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE session (id INT AUTO_INCREMENT NOT NULL, emprunt_id INT, description LONGTEXT NOT NULL, nb_min INT NOT NULL, nb_max INT NOT NULL, duree INT NOT NULL, lieu VARCHAR(255) NOT NULL, date DATE NOT NULL, heuredebut TIME NOT NULL, heurefin TIME NOT NULL, INDEX IDX_D044D5D4AE7FEF94 (emprunt_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session_user (session_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_4BE2D663613FECDF (session_id), INDEX IDX_4BE2D663A76ED395 (user_id), PRIMARY KEY(session_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posseder (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, jeu_id INT NOT NULL, niveau INT NOT NULL, tuteur INT NOT NULL, nb_exemplaire INT NOT NULL, preferences VARCHAR(255) NOT NULL, annee_acquisition INT DEFAULT NULL, etat VARCHAR(255) DEFAULT NULL, INDEX IDX_62EF7CBAA76ED395 (user_id), INDEX IDX_62EF7CBA8C9E392E (jeu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `group` (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) NOT NULL UNIQUE, description LONGTEXT NOT NULL, public TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, pseudo VARCHAR(255) NOT NULL UNIQUE, email VARCHAR(255) NOT NULL, lieu VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, privileges VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE jeu (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) NOT NULL, auteur VARCHAR(255) NOT NULL, editeur VARCHAR(255) NOT NULL, annee_edition INT NOT NULL, duree_partie INT NOT NULL, type VARCHAR(255) NOT NULL, complexite INT NOT NULL, nb_min INT NOT NULL, nb_max INT NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inclure (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, grup_id INT NOT NULL, responsable TINYINT(1) NOT NULL, INDEX IDX_6C35D95CA76ED395 (user_id), INDEX IDX_6C35D95C569AD2DE (grup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emprunter (id INT AUTO_INCREMENT NOT NULL, jeu_id INT NOT NULL, date_emprunt DATE NOT NULL, date_retour DATE NOT NULL, INDEX IDX_E23B93F8C9E392E (jeu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D4AE7FEF94 FOREIGN KEY (emprunt_id) REFERENCES emprunter (id)');
        $this->addSql('ALTER TABLE session_user ADD CONSTRAINT FK_4BE2D663613FECDF FOREIGN KEY (session_id) REFERENCES session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_user ADD CONSTRAINT FK_4BE2D663A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE posseder ADD CONSTRAINT FK_62EF7CBAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE posseder ADD CONSTRAINT FK_62EF7CBA8C9E392E FOREIGN KEY (jeu_id) REFERENCES jeu (id)');
        $this->addSql('ALTER TABLE inclure ADD CONSTRAINT FK_6C35D95CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE inclure ADD CONSTRAINT FK_6C35D95C569AD2DE FOREIGN KEY (grup_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE emprunter ADD CONSTRAINT FK_E23B93F8C9E392E FOREIGN KEY (jeu_id) REFERENCES jeu (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE session_user DROP FOREIGN KEY FK_4BE2D663613FECDF');
        $this->addSql('ALTER TABLE inclure DROP FOREIGN KEY FK_6C35D95C569AD2DE');
        $this->addSql('ALTER TABLE session_user DROP FOREIGN KEY FK_4BE2D663A76ED395');
        $this->addSql('ALTER TABLE posseder DROP FOREIGN KEY FK_62EF7CBAA76ED395');
        $this->addSql('ALTER TABLE inclure DROP FOREIGN KEY FK_6C35D95CA76ED395');
        $this->addSql('ALTER TABLE posseder DROP FOREIGN KEY FK_62EF7CBA8C9E392E');
        $this->addSql('ALTER TABLE emprunter DROP FOREIGN KEY FK_E23B93F8C9E392E');
        $this->addSql('ALTER TABLE session DROP FOREIGN KEY FK_D044D5D4AE7FEF94');
        $this->addSql('DROP TABLE session');
        $this->addSql('DROP TABLE session_user');
        $this->addSql('DROP TABLE posseder');
        $this->addSql('DROP TABLE `group`');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE jeu');
        $this->addSql('DROP TABLE inclure');
        $this->addSql('DROP TABLE emprunter');
    }
}
