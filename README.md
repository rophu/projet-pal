# projet PAL

#Si le fichier .env n'existe pas, le créer dans serveur/
touch .env
# y placer le contenu suivant

###> symfony/framework-bundle ###
APP_ENV=dev
APP_SECRET=b67afcb9e643425f1c4968f17416b26c
#TRUSTED_PROXIES=127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
#TRUSTED_HOSTS='^(localhost|example\.com)$'
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# For a PostgreSQL database, use: "postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8"
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
###< doctrine/doctrine-bundle ###

###> nelmio/cors-bundle ###
CORS_ALLOW_ORIGIN=*
###< nelmio/cors-bundle ###


#puis dans le .env la racine du projet, remplacer
# db_user par votre nom d'utilsateur mysql
# db_password par votre mot de passe mysql
# db_name par le nom de base de donnée que vous souhaitez utiliser

#Pour installer le projet, une fois le projet cloné via git lancer 
#dans le dossier serveur/ :

composer require doctrine/annotations

composer require nelmio/cors-bundle

composer require fzaninotto/faker

composer req --dev doctrine/doctrine-fixtures-bundle

#De retour dans un terminal dans le dossier server/
#Si la base de données n'existe pas,

php bin/console doctrine:database:create

#Faire la mise à jour de la bdd avec 

php bin/console doctrine:schema:update --force

#Faire tourner les fixtures pour peupler la base 
#Ou bien utiliser le script Postman pour tester les fonctionnalité de l'API

php bin/console doctrine:fixtures:load
