Les Sessions

Ce fichier regroupe toutes les informations à propos des routes de l'API qui concernent
les Sessions

* (1) fetch (GET) all sessions : /PAL/api/v1.0/session
* (2) fetch (GET) one session with ID : /PAL/api/v1.0/session/id
* (3) create (POST) one session : /PAL/api/v1.0/session
* (4) modifiyng (PUT) one session : /PAL/api/v1.0/session/id
* (5) delete (DELETE) one session : /PAL/api/v1.0/session
* (6) add (PUT) an user to a session : /PAL/api/v1.0/session/addUser/id
* (7) remove (PUT) an user to a session : /PAL/api/v1.0/session/removeUser/id

(1) Fetch all sessions:
URL : /PAL/api/v1.0/session
Method: GET
URL Params : No params

Success Response:
Code: 200
Content:
{
    "jeu": [
        {
            "id": 36,
            "description": "Explicabo harum sunt vel voluptatem quia dolores reprehenderit autem sit fuga delectus quia.",
            "nb_min": 2,
            "nb_max": 4,
            "duree": 120,
            "lieu": "29 rue des acacias Orléans",
            "date": {
                "date": "1988-07-11 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "heureDebut": {
                "date": "1970-01-01 14:22:06.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "heureFin": {
                "date": "1970-01-01 23:42:48.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "jeu_id": 52,
            "jeu_titre": 52,
            "dateEmprunt": {
                "date": "2020-04-17 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "dateRetour": {
                "date": "2021-06-02 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "participants": [
                {
                    "user_id": 21
                },
                {
                    "user_id": 22
                }
            ]
        }
	]
}
Error Response:
Pas d'erreur pour le moment...

(2) fetch one sessions with id:
URL : /PAL/api/v1.0/session/id
Method: GET
URL Params:
Required:
id=[integer]
no optional params

Success Response:
Code: 200
Content:
{
    "id": 38,
    "description": "Nam officia perferendis cupiditate qui error doloribus.",
    "nb_min": 1,
    "nb_max": 3,
    "duree": 120,
    "lieu": "salle du temps perdu 39 rue des étalons Orléans",
    "date": {
        "date": "1974-09-18 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureDebut": {
        "date": "1970-01-01 15:09:22.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureFin": {
        "date": "1970-01-01 16:57:49.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "jeu_id": 51,
    "jeu_titre": "laborum",
    "dateEmprunt": {
        "date": "2020-05-03 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "dateRetour": {
        "date": "2021-06-02 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "participants": []
}
Error Response:
Code: 500
Si l'id n'existe pas

Notes:
Pas encore de gestion d'erreur si jamais l'ID n'existe pas en BDD

(3) create a new session:
URL : /PAL/api/v1.0/session
Method : POST
URL Params: no Params

Data Params:
emprunt_id=[integer]
description=[long text]
nbMin=[integer]
nbMax=[integer]
duree=[integer]
date=[String] 		/Format (YYYY-MM-DD)
heureDebut=[String]	/Format (HH:mm:ss)
heureFin=[String]	/Format (HH:mm:ss)

Postman Body Example:
{
	"emprunt_id":"1",
	"titre":"The best Session Ever",
	"description":"Une session comme au bon vieux temps",
	"nb_min":"3",
	"nb_max":"15",
	"duree":"360",
	"lieu":"Orlêans",
	"date":"2020-07-14",
	"heureDebut":"16:00:00",
	"heureFin":"22:00:00"
}

Success Response:
Code: 200
Content:
{
    "emprunt_id": 12,
    "description": "Une session comme au bon vieux temps",
    "nbMin": 3,
    "nbMax": 15,
    "Duree": 360,
    "Lieu": "Orlêans",
    "Date": {
        "date": "2020-07-14 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureDebut": {
        "date": "2020-07-14 16:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureFin": {
        "date": "2020-07-14 22:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "participants": []
}

(4) Modifiyng a session:
URL : /PAL/api/v1.0/session/id
Method : PUT
URL Params:
Required:
id=[integer]
no Optional Params

Data Params:
emprunt_id=[integer]
description=[long text]
nbMin=[integer]
nbMax=[integer]
duree=[integer]
date=[String] 		/Format (YYYY-MM-DD)
heureDebut=[String]	/Format (HH:mm:ss)
heureFin=[String]	/Format (HH:mm:ss)

Postman Body Example:
{
	"emprunt_id":"1",
	"titre":"The best Session Ever",
	"description":"Une session comme au bon vieux temps",
	"nb_min":"3",
	"nb_max":"15",
	"duree":"360",
	"lieu":"Orléans",
	"date":"2020-07-14",
	"heureDebut":"16:00:00",
	"heureFin":"22:00:00"
}

Success Response:
Code: 200
Content:
{
    "emprunt_id": 12,
    "description": "Une session comme au bon vieux temps",
    "nbMin": 3,
    "nbMax": 15,
    "Duree": 360,
    "Lieu": "Orlêans",
    "Date": {
        "date": "2020-07-14 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureDebut": {
        "date": "2020-07-14 16:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureFin": {
        "date": "2020-07-14 22:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "participants": [
        {
            "user_id": 21
        },
        {
            "user_id": 22
        }
    ]
}
Error Response:
Code: 500
Si manque un attribut ou que l'ID n'existe pas

Notes:
Pas de gestion d'erreur...

(5) Delete a session:
URL : /PAL/api/v1.0/session
Method : DELETE
URL Params: no params

Data Params:
id=[integer]

Postman Body Example:
{
	"id":12
}
Success Response:
Code: 200
Content:
{
    "id": null,
    "description": "Omnis maxime dolore optio et nihil fuga repellat perspiciatis deserunt sint.",
    "nb_min": 1,
    "nb_max": 8,
    "duree": 150,
    "lieu": "191 rue des murets Saran",
    "date": {
        "date": "2016-10-22 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureDebut": {
        "date": "1970-01-01 19:56:06.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureFin": {
        "date": "1970-01-01 08:05:13.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "jeu_id": 52,
    "jeu_titre": "sit",
    "dateEmprunt": {
        "date": "2019-10-03 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "dateRetour": {
        "date": "2021-06-02 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "participants": []
}
Error Response:
Code: 500
Si l'id n'existe pas

Notes: 
Pas de gestion d'erreur pour le moment
Contrairement à User, et Jeu, Session peut être supprimé même si un emprunt
ou des participants existe. /!\

(6) add an user to a session:
URL : /PAL/api/v1.0/session/addUser/id
Method : PUT
URL Params:
Required:
id=[integer]
no Optional Params

Data Params:
user_id=[integer]

Postman Body Example:
{
	"user_id":24
}

Success Response:
Code: 200
Content:
{
    "emprunt_id": 12,
    "description": "Une session comme au bon vieux temps",
    "nbMin": 3,
    "nbMax": 15,
    "Duree": 360,
    "Lieu": "Orlêans",
    "Date": {
        "date": "2020-07-14 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureDebut": {
        "date": "1970-01-01 16:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureFin": {
        "date": "1970-01-01 22:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "participants": [
        {
            "user_id": 21
        },
        {
            "user_id": 22
        },
        {
            "user_id": 24
        }
    ]
}

Error Response:
Code: 500
Si manque un attribut ou que l'id n'existe pas

Notes:
Pas de gestion d'erreur...

(8) remove an user from a session :
URL : /PAL/api/v1.0/session/remoevUser/id
Method : PUT
URL Params:
Required:
id=[integer]
no Optional Params

Data Params:
session_id=[integer]

Postman Body Example:
{
	"session_id":23
}
Success Response:
Code: 200
{
    "emprunt_id": 12,
    "description": "Une session comme au bon vieux temps",
    "nbMin": 3,
    "nbMax": 15,
    "Duree": 360,
    "Lieu": "Orlêans",
    "Date": {
        "date": "2020-07-14 00:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureDebut": {
        "date": "1970-01-01 16:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "heureFin": {
        "date": "1970-01-01 22:00:00.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
    },
    "participants": [
        {
            "user_id": 21
        },
        {
            "user_id": 22
        }
    ]
}

Error Response:
Code: 500
Si manque un attribut ou que l'id n'existe pas

Notes:
Pas de gestion d'erreur...

Documentation Rédigé par Adzuar Rémy the Salted Pickle
le 2020-06-04

          \ /
          oVo
      \___XXX___/
       __XXXXX__
      /__XXXXX__\
      /   XXX   \
           V
