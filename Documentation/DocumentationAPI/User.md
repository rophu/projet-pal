Les Users

Ce fichier regroupe toutes les informations à propos des routes de l'API qui concernent
les utilisateurs

* (1) fetch (GET) all users : /PAL/api/v1.0/user
* (2) fetch (GET) one user with ID : /PAL/api/v1.0/user/id
* (3) fetch (GET) one user with Pseudo : /PAL/api/v1.0/user/details/pseudo
* (4) create (POST) one user : /PAL/api/v1.0/user
* (5) modifiyng (PUT) one user : /PAL/api/v1.0/user/id
* (6) delete (DELETE) one user : /PAL/api/v1.0/user
* (7) add (PUT) a session to an user : /PAL/api/v1.0/user/addSession/id
* (8) remove (PUT) a session to an user : /PAL/api/v1.0/user/removeSession/id

(1) Fetch all groups:
URL : /PAL/api/v1.0/user
Method: GET
URL Params : No params

Success Response:
Code: 200
Content:
{
    "user": [
        {
            "id": 21,
            "pseudo": "Myriam",
            "email": "rgerlach@kuhlman.biz",
            "lieu": "Saint-Jean-Le-Blanc",
            "description": "Deleniti optio ut voluptatem minus laborum quae sunt.",
            "privileges": "dieu des échecs",
            "listeJeux": [
                {
                    "jeu_id": 51,
                    "jeu_titre": "laborum",
                    "jeu_niveau": 1,
                    "jeu_tuteur": 1,
                    "jeu_nb_exemplaire": 0,
                    "jeu_preferences": "passionément",
                    "jeu_anneeAcquisition": 2012,
                    "jeu_getEtat": "Bon"
                }
            ],
            "listeGroup": [],
            "listeSession": [
                {
                    "session_id": "Explicabo harum sunt vel voluptatem quia dolores reprehenderit autem sit fuga delectus quia.",
                    "session_description": "Explicabo harum sunt vel voluptatem quia dolores reprehenderit autem sit fuga delectus quia.",
                    "session_nb_min": 2,
                    "session_nb_max": 4,
                    "session_duree": 120,
                    "session_lieu": "29 rue des acacias Orléans",
                    "session_date": {
                        "date": "1988-07-11 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "session_participer": {},
                    "session_heuredebut": {
                        "date": "1970-01-01 14:22:06.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "session_heurefin": {
                        "date": "1970-01-01 23:42:48.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    }
                }
            ]
        }
	]
}
Error Response:
Pas d'erreur pour le moment...

Notes: Le Json recu est assez lourd mais assez exhaustif pour effectuer une
multitude de traitement, tirez en parti! Mais ça peut évoluer

(2) Fetch one user with the ID:
URL : /PAL/api/v1.0/user/id
Method: GET
URL Params:
Required:
id=[integer]
no optional params

Success Response:
Code: 200
Content:
{
    "id": 23,
    "pseudo": "Ashlee",
    "email": "ankunding.edward@braun.com",
    "lieu": "Orléans",
    "description": "Voluptates consequatur provident magnam labore cum quibusdam.",
    "privileges": "Imperium gamemaster",
    "listeJeux": [
        {
            "jeu_id": 51,
            "jeu_titre": "laborum",
            "jeu_niveau": 7,
            "jeu_tuteur": 0,
            "jeu_nb_exemplaire": 1,
            "jeu_preferences": "beaucoup",
            "jeu_anneeAcquisition": 2020,
            "jeu_getEtat": "déplorable"
        }
    ],
    "listeGroup": [
        {
            "group_id": 11,
            "group_titre": "nesciunt",
            "group_description": "Quas natus occaecati tempora fuga fugiat ut.",
            "group_public": false
        }
    ],
    "listeSession": []
}
Error Response:
Code: 500
Si l'id n'existe pas

Notes:
Pas encore de gestion d'erreur si jamais l'ID n'existe pas en BDD

(3) Fetch one user with the pseudo:
URL : /PAL/api/v1.0/user/details/pseudo
Method: GET
URL Params:
Required:
id=[integer]
no optional params

Success Response:
Code: 200
Content:
{
    "id": 23,
    "pseudo": "Ashlee",
    "email": "ankunding.edward@braun.com",
    "lieu": "Orléans",
    "description": "Voluptates consequatur provident magnam labore cum quibusdam.",
    "privileges": "Imperium gamemaster",
    "listeJeux": [
        {
            "jeu_id": 51,
            "jeu_titre": "laborum",
            "jeu_niveau": 7,
            "jeu_tuteur": 0,
            "jeu_nb_exemplaire": 1,
            "jeu_preferences": "beaucoup",
            "jeu_anneeAcquisition": 2020,
            "jeu_getEtat": "déplorable"
        }
    ],
    "listeGroup": [
        {
            "group_id": 11,
            "group_titre": "nesciunt",
            "group_description": "Quas natus occaecati tempora fuga fugiat ut.",
            "group_public": false
        }
    ],
    "listeSession": []
}
Error Response:
Code: 500
Si le pseudo de l'utilisateur n'existe pas

Notes:
Pas de gestion d'erreur si l'utilisateur n'existe pas en BDD

(4) Create a new user :
URL : /PAL/api/v1.0/user
Method : POST
URL Params: no Params

Data Params:
pseudo=[String]
email=[String]
lieu=[String]
description=[Long text]
privileges=[String]

Postman Body Example:
{
	"pseudo":"Bob",
	"email":"Marley@wailers.com",
	"lieu":"Kingston",
	"description":"Fan de jeu de ròle",
	"privileges":"aucuns"
}

Success Response:
Code: 200
Content:
{
    "pseudo": "Bob",
    "email": "Marley@wailers.com",
    "lieu": "Kingston",
    "description": "Fan de jeu de ròle",
    "privileges": "aucuns"
}
Error Response:
Code: 500
Si manque un attribut

Notes:
Pas de gestion d'erreur...

(5) Modifiyng one User:
URL : /PAL/api/v1.0/user/id
Method : PUT
URL Params:
Required:
id=[integer]
no Optional Params

Data Params:
pseudo=[String]
email=[String]
lieu=[String]
description=[Long text]
privileges=[String]

Postman Body Example:
{
	"pseudo":"Bob",
	"email":"Marley@wailers.com",
	"lieu":"Kingston",
	"description":"Fan de jeu de ròle",
	"privileges":"aucuns"
}

Success Response:
Code: 200
Content:
{
    "pseudo": "Bob",
    "email": "Marley@wailers.com",
    "lieu": "Kingston",
    "description": "Fan de jeu de ròle",
    "privileges": "aucuns"
}

Error Response:
Code: 500
Si manque un attribut

Notes:
Pas de gestion d'erreur...

(6) Delete one User:
URL : /PAL/api/v1.0/user
Method : DELETE
URL Params: no params

Data Params:
id=[integer]

Postman Body Example:
{
	"id":12
}
Success Response:
Code: 200
Content:
{
    "id": null,
    "pseudo": "Bob",
    "email": "Marley@wailers.com",
    "lieu": "Kingston",
    "description": "Fan de jeu de ròle",
    "privileges": "aucuns"
}
Error Response:
Code: 500
Si l'id n'existe pas

Notes: 
Pas de gestion d'erreur pour le moment
/!\ Supprimer une instance de joueur n'est pas sensé arrivé, la fonctionnalité
va peut être disparaitre dans une futur version de l'API /!\
/!\ Supprimer une instance de joueur, suppose d'avoir supprimer tout ces 
groupes, sessions et jeux, ce qui n'est pas encore pris en charge automatiquement/!\

(7) Add a session to an User :
URL : /PAL/api/v1.0/user/addSession/id
Method : PUT
URL Params:
Required:
id=[integer]
no Optional Params

Data Params:
session_id=[integer]

Postman Body Example:
{
	"session_id":23
}

Success Response:
Code: 200
Content:
{
    "id": 24,
    "pseudo": "Alayna",
    "email": "abbott.keara@yahoo.com",
    "lieu": "Fleury-les-Aubraies",
    "description": "Doloremque asperiores consequuntur alias impedit architecto voluptas suscipit alias.",
    "privileges": "Nain jaune",
    "listeJeux": [
        {
            "jeu_id": 52,
            "jeu_titre": "sit",
            "jeu_niveau": 9,
            "jeu_tuteur": 0,
            "jeu_nb_exemplaire": 1,
            "jeu_preferences": "à la folie",
            "jeu_anneeAcquisition": 2017,
            "jeu_getEtat": "presque pas abimé"
        }
    ],
    "listeGroup": [
        {
            "group_id": 12,
            "group_titre": "et",
            "group_description": "Excepturi porro in necessitatibus molestias suscipit rerum et consequatur rerum cum architecto vitae.",
            "group_public": false
        }
    ],
    "listeSession": [
        {
            "session_id": 23,
            "session_description": "Nam officia perferendis cupiditate qui error doloribus.",
            "session_nb_min": 1,
            "session_nb_max": 3,
            "session_duree": 120,
            "session_lieu": "salle du temps perdu 39 rue des étalons Orléans",
            "session_date": {
                "date": "1974-09-18 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "session_participer": {},
            "session_heuredebut": {
                "date": "1970-01-01 15:09:22.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "session_heurefin": {
                "date": "1970-01-01 16:57:49.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            }
        }
    ]
}
Error Response:
Code: 500
Si manque un attribut ou que l'id n'existe pas

Notes:
Pas de gestion d'erreur...

(8) remove a session to an User:
URL : /PAL/api/v1.0/user/removeSession/id
Method : PUT
URL Params:
Required:
id=[integer]
no Optional Params

Data Params:
session_id=[integer]

Postman Body Example:
{
	"session_id":23
}
Success Response:
Code: 200
{
    "id": 24,
    "pseudo": "Alayna",
    "email": "abbott.keara@yahoo.com",
    "lieu": "Fleury-les-Aubraies",
    "description": "Doloremque asperiores consequuntur alias impedit architecto voluptas suscipit alias.",
    "privileges": "Nain jaune",
    "listeJeux": [
        {
            "jeu_id": 52,
            "jeu_titre": "sit",
            "jeu_niveau": 9,
            "jeu_tuteur": 0,
            "jeu_nb_exemplaire": 1,
            "jeu_preferences": "à la folie",
            "jeu_anneeAcquisition": 2017,
            "jeu_getEtat": "presque pas abimé"
        }
    ],
    "listeGroup": [
        {
            "group_id": 12,
            "group_titre": "et",
            "group_description": "Excepturi porro in necessitatibus molestias suscipit rerum et consequatur rerum cum architecto vitae.",
            "group_public": false
        }
    ],
    "listeSession": []
}

Error Response:
Code: 500
Si manque un attribut ou que l'id n'existe pas

Notes:
Pas de gestion d'erreur...

Documentation Rédigé par Adzuar Rémy the Salted Pickle
le 2020-06-04

