Les Jeux

Ce fichier regroupe toutes les informations à propos des routes de l'API qui concernent
les utilisateurs

* (1) fetch (GET) all jeux : /PAL/api/v1.0/jeu
* (2) fetch (GET) one jeu with ID : /PAL/api/v1.0/jeu/id
* (3) fetch (GET) one jeu with titre : /PAL/api/v1.0/jeu/details/id
* (4) create (POST) one jeu : /PAL/api/v1.0/jeu
* (5) modifiyng (PUT) one jeu : /PAL/api/v1.0/jeu/id
* (6) delete (DELETE) one jeu : /PAL/api/v1.0/jeu

(1) Fetch all users:
URL : /PAL/api/v1.0/jeu
Method: GET
URL Params : No params

Success Response:
Code: 200
Content:
{
    "jeu": [
        {
            "id": 51,
            "titre": "laborum",
            "auteur": "Aaliyah Will",
            "editeur": "Guillotine games",
            "anneeEdition": 2000,
            "$dureePartie": 120,
            "type": "Stratégie",
            "complexite": 4,
            "nbMin": 1,
            "nbMax": 7,
            "description": "Quia quam itaque voluptates sed dolores delectus ipsum ut in illo nemo.",
            "posseder": [
                {
                    "user_id": 21,
                    "user_pseudo": "Bob",
                    "posseder_niveau": 1,
                    "posseder_tuteur": 1,
                    "posseder_nb_exemplaire": 0,
                    "posseder_preferences": "passionément",
                    "posseder_annee_acquisition": 2012,
                    "posseder_etat": "Bon"
                },
                {
                    "user_id": 23,
                    "user_pseudo": "Ashlee",
                    "posseder_niveau": 7,
                    "posseder_tuteur": 0,
                    "posseder_nb_exemplaire": 1,
                    "posseder_preferences": "beaucoup",
                    "posseder_annee_acquisition": 2020,
                    "posseder_etat": "déplorable"
                }
            ],
            "emprunts": [
                {
                    "emprunt_dateEmprunt": {
                        "date": "2020-05-03 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "emprunt_dateRetour": {
                        "date": "2021-06-02 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "sessions": [
                        {
                            "session_description": "Nam officia perferendis cupiditate qui error doloribus."
                        },
                        {
                            "session_description": "In iure aliquid eius delectus et et."
                        }
                    ]
                },
                {
                    "emprunt_dateEmprunt": {
                        "date": "2019-08-15 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "emprunt_dateRetour": {
                        "date": "2021-06-02 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "sessions": []
                },
                {
                    "emprunt_dateEmprunt": {
                        "date": "2019-06-12 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "emprunt_dateRetour": {
                        "date": "2021-06-02 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "sessions": []
                }
            ]
        }
	]
}
Error Response:
Pas d'erreur pour le moment...

(2) Fetch one jeu with id:
URL : /PAL/api/v1.0/jeu/id
Method: GET
URL Params:
Required:
id=[integer]
no optional params

Success Response:
Code: 200
Content:
{
    "id": 52,
    "titre": "sit",
    "auteur": "Pinkie Wisoky",
    "editeur": "Guillotine games",
    "anneeEdition": 2000,
    "$dureePartie": 180,
    "type": "Stratégie",
    "complexite": 2,
    "nbMin": 2,
    "nbMax": 8,
    "description": "Repellendus commodi assumenda quia beatae pariatur assumenda nisi rerum amet.",
    "posseder": [
        {
            "user_id": 22,
            "user_pseudo": "Everette",
            "posseder_niveau": 7,
            "posseder_tuteur": 0,
            "posseder_nb_exemplaire": 2,
            "posseder_preferences": "passionément",
            "posseder_annee_acquisition": 2016,
            "posseder_etat": "presque pas abimé"
        },
        {
            "user_id": 24,
            "user_pseudo": "Alayna",
            "posseder_niveau": 9,
            "posseder_tuteur": 0,
            "posseder_nb_exemplaire": 1,
            "posseder_preferences": "à la folie",
            "posseder_annee_acquisition": 2017,
            "posseder_etat": "presque pas abimé"
        }
    ],
    "emprunts": [
        {
            "emprunt_dateEmprunt": {
                "date": "2020-04-17 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "emprunt_dateRetour": {
                "date": "2021-06-02 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "sessions": [
                {
                    "session_description": "Explicabo harum sunt vel voluptatem quia dolores reprehenderit autem sit fuga delectus quia."
                },
                {
                    "session_description": "Qui nulla ex est dolor est aut accusamus voluptatem corrupti."
                }
            ]
        },
        {
            "emprunt_dateEmprunt": {
                "date": "2019-10-03 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "emprunt_dateRetour": {
                "date": "2021-06-02 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "sessions": [
                {
                    "session_description": "Expedita velit voluptas necessitatibus nobis quo recusandae vitae dignissimos nemo odio amet."
                },
                {
                    "session_description": "Omnis maxime dolore optio et nihil fuga repellat perspiciatis deserunt sint."
                }
            ]
        },
        {
            "emprunt_dateEmprunt": {
                "date": "2020-02-14 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "emprunt_dateRetour": {
                "date": "2021-06-02 00:00:00.000000",
                "timezone_type": 3,
                "timezone": "Europe/Berlin"
            },
            "sessions": []
        }
    ]
}
Error Response:
Code: 500
Si l'id n'existe pas

Notes:
Pas encore de gestion d'erreur si jamais l'ID n'existe pas en BDD

(3) Fetch one jeu with the titre:
URL : /PAL/api/v1.0/jeu/details/id
Method : GET
URL Params:
Required:
id=[integer]
no optional params

Success Response:
Code: 200
Content:
{
    "jeux": [
        {
            "id": 52,
            "titre": "sit",
            "auteur": "Pinkie Wisoky",
            "editeur": "Guillotine games",
            "anneeEdition": 2000,
            "$dureePartie": 180,
            "type": "Stratégie",
            "complexite": 2,
            "nbMin": 2,
            "nbMax": 8,
            "description": "Repellendus commodi assumenda quia beatae pariatur assumenda nisi rerum amet.",
            "posseder": [
                {
                    "user_id": 22,
                    "user_pseudo": "Everette",
                    "posseder_niveau": 7,
                    "posseder_tuteur": 0,
                    "posseder_nb_exemplaire": 2,
                    "posseder_preferences": "passionément",
                    "posseder_annee_acquisition": 2016,
                    "posseder_etat": "presque pas abimé"
                }
            ],
            "emprunts": [
                {
                    "emprunt_dateEmprunt": {
                        "date": "2020-04-17 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "emprunt_dateRetour": {
                        "date": "2021-06-02 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "sessions": [
                        {
                            "session_description": "Explicabo harum sunt vel voluptatem quia dolores reprehenderit autem sit fuga delectus quia."
                        },
                        {
                            "session_description": "Qui nulla ex est dolor est aut accusamus voluptatem corrupti."
                        }
                    ]
                },
                {
                    "emprunt_dateEmprunt": {
                        "date": "2019-10-03 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "emprunt_dateRetour": {
                        "date": "2021-06-02 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "sessions": [
                        {
                            "session_description": "Expedita velit voluptas necessitatibus nobis quo recusandae vitae dignissimos nemo odio amet."
                        },
                        {
                            "session_description": "Omnis maxime dolore optio et nihil fuga repellat perspiciatis deserunt sint."
                        }
                    ]
                },
                {
                    "emprunt_dateEmprunt": {
                        "date": "2020-02-14 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "emprunt_dateRetour": {
                        "date": "2021-06-02 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "Europe/Berlin"
                    },
                    "sessions": []
                }
            ]
        }
    ]
}
Error Response:
Code: 500
Si le titre du jeu n'existe pas

Notes:
Pas de gestion d'erreur si le titre du jeu n'est pas en BDD

(4) Create a new jeu:
URL : /PAL/api/v1.0/jeu
Method : POST
URL Params: no Params

Data Params:
titre=[String]
auteur=[String]
editeur=[String]
anneeEdition=[integer]
dureePartie=[integer] /en minutes
type=[String]
complexite=[integer] /sur echelle de 5
nbMin=[integer]		 /nb joueur minimum
nbMax=[integer]		 /nb joueur maximum
description=[Long text]

Postman Body Example:
{
	"titre":"L'apatosaure",
	"auteur":"Gaia",
	"editeur":"Univers C137",
	"anneeEdition":"1999",
	"dureePartie":"120",
	"type":"Jeu de Rôle",
	"complexite":"4",
	"nbMin":"3",
	"nbMax":"6",
	"description":"Un jeu de ròle ou vous incarnez un apatosaure"
}

Success Response:
Code: 200
Content:
{
	"id": 23,
    "titre": "L'apatosaure",
    "auteur": "Gaia",
    "editeur": "Univers C137",
    "anneeEdition": 1999,
    "dureePartie": 120,
    "type": "Jeu de Rôle",
    "complexite": 4,
    "nbMin": 3,
    "nbMax": 6,
    "description": "Un jeu de ròle ou vous incarnez un apatosaure",
    "posseder": [],
    "emprunts": []
}
Error Response:
Code: 500
Si manque un attribut

Notes:
Pas de gestion d'erreur...

(5) Modifiyng un Jeu:
URL : /PAL/api/v1.0/jeu/id
Method : PUT
URL Params:
Required:
id=[integer]
no Optional Params

Data Params:
titre=[String]
auteur=[String]
editeur=[String]
anneeEdition=[integer]
dureePartie=[integer] /en minutes
type=[String]
complexite=[integer] /sur echelle de 5
nbMin=[integer]		 /nb joueur minimum
nbMax=[integer]		 /nb joueur maximum
description=[Long text]

Postman Body Example:
{
    "titre": "L'apatosaure",
    "auteur": "Gaia",
    "editeur": "Univers C137",
    "anneeEdition": 1999,
    "dureePartie": 120,
    "type": "Jeu de Rôle",
    "complexite": 4,
    "nbMin": 3,
    "nbMax": 6,
    "description": "Un jeu de ròle ou vous incarnez un apatosaure"
}

Success Response:
Code: 200
Content:
{
	"id": 23,
    "titre": "L'apatosaure",
    "auteur": "Gaia",
    "editeur": "Univers C137",
    "anneeEdition": 1999,
    "dureePartie": 120,
    "type": "Jeu de Rôle",
    "complexite": 4,
    "nbMin": 3,
    "nbMax": 6,
    "description": "Un jeu de ròle ou vous incarnez un apatosaure",
    "posseder": [],
    "emprunts": []
}

Error Response:
Code: 500
Si manque un attribut

Notes:
Pas de gestion d'erreur...

(6) Delete one jeu:
URL : /PAL/api/v1.0/jeu
Method : DELETE
URL Params: no params

Data Params:
id=[integer]

Postman Body Example:
{
	"id":106
}
Success Response:
Code: 200
Content:
{
    "id": null,
    "titre": "Super Jeu Mario",
    "auteur": "Nathan",
    "editeur": "gallimard",
    "anneeEdition": 2222,
    "dureePartie": 150,
    "type": "casual",
    "complexite": 1,
    "nbMin": 2,
    "nbMax": 5,
    "description": "PLOPLIPPLAP"
}
Error Response:
Code: 500
Si l'id n'existe pas

Notes: 
Pas de gestion d'erreur pour le moment
/!\ Supprimer une instance de jeu n'est pas sensé arrivé, la fonctionnalité
va peut être disparaitre dans une futur version de l'API /!\
/!\ Supprimer une instance de jeu, suppose d'avoir supprimer tout ces 
user le possedant, et tout les session l'empruntant
, ce qui n'est pas encore pris en charge automatiquement/!\

Documentation Rédigé par Adzuar Rémy the fearful frog
le 2020-06-04
               __
              / _)
     _.----._/ /
    /         /
 __/ (  | (  |
/__.-'|_|--|_|
