Les Groupes

Ce fichier regroupe toute les informations à propos des routes de l'API qui coroupes :
* (1) fetch (GET) all groups : /PAL/api/v1.0/group 
* (2) fetch (GET) one group with the ID : /PAL/api/v1.0/group/id
* (3) fetch (GET) one group with the title : /PAL/api/v1.0/group/details/title
* (4) create (POST) one group : /PAL/api/v1.0/group
* (5) modifiyng (PUT) one group : /PAL/api/v1.0/group/id
* (6) delete (DELETE) one group : /PAL/api/v1.0/group

(1)Fetch all groups :
URL : /PAL/api/v1.0/group 
Method: GET
URL Params : No params

Success Response:
Code: 200
Content: 
{
    "group": [
        {
            "id": 10,
            "titre": "aut",
            "description": "Eaque maxime ... voluptatem.",
            "public": false,
            "userInclus": [
                {
                    "user_id": 22,
                    "user_pseudo": "Everette",
                    "responsable": false
                }
            ]
        }
    ]
]
Error Response:
Pas d'erreur pour le moment...

Notes: Ø

(2)Fetch one group with the ID:
URL : /PAL/api/v1.0/group/id
Method: GET
URL Params:
Required:
id=[integer]
no Optional params

Success Response:
Code: 200
Content:
{
    "id": 13,
    "titre": "neque",
    "description": "Nobis voluptatum ... plop.",
    "public": false,
    "users": [
        {
            "user_id": 25,
            "user_pseudo": "Matteo",
            "responsable": false
        }
    ]
}
Error Response:
Code: 500
Si l'id n'existe pas

Notes:
Pas encore de gestion d'erreur si jamais l'ID n'existe pas en BDD

(3)Fetch one group with the title:
URL : /PAL/api/v1.0/group/details/title
Method : GET
URL Params:
Required:
id=[integer]
no optional params

Success Response:
Code: 200
Content:
{
    "id": 10,
    "titre": "aut",
    "description": "Eaque maxime voluptatem consequuntur earum laborum laborum deleniti optio magnam quod animi voluptatem.",
    "public": false,
    "users": [
        {
            "user_id": 22,
            "user_pseudo": "Everette",
            "responsable": false
        }
    ]
}
Error Response:
Code: 500
Si le titre de groupe n'existe pas

Notes:
Pas de gestion d'erreur si le groupe n'existe pas en BDD

(4) Create a new group:
URL : /PAL/api/v1.0/group
Method: POST
URL Params: No Params

Data Params:
titre=[String]
description=[long text]
public=[boolean]

Postman Body Example:
{
	"titre":"plop",
	"description":"plop plip plap",
	"public":true
}

Success Response:
Code: 200
Content:
{
    "id": 17,
    "titre": "plop",
    "description": "plop plip plap",
    "public": true,
    "users": []
}

Error Response:
Code: 500
Si manque un attribut

Notes:
Pas de gestion d'erreur pour le moment donc retourne toujours 500 si erreur

(5) Modifiyng one group:
URL : /PAL/api/v1.0/group/id
Method : PUT
URL Params:
Required:
id=[integer]
no Optional Params

Data Params:
titre=[String]
description=[long text]
public=[boolean]

Postman Body Example:
{
    "titre": "plop",
    "description": "plop plap plup",
    "public": true,
    "users": []
}

Success Response:
Code: 200
Content:
{
    "id": 17,
    "titre": "plop",
    "description": "plop plap plup",
    "public": true,
    "users": []
}
Error Response:
Code: 500
Si manque un attribut

Notes:
Pas de gestion d'erreur...

(6) Delete one Group:
URL : /PAL/api/v1.0/group
Method : DELETE
URL Params: no params

Data Params:
id=[integer]

Postman Body Example:
{
	"id":12
}
Success Response:
Code: 200
Content:
{
    "id": null,
    "titre": "plop",
    "description": "plop plip plap",
    "users": []
}

Error Response:
Code: 500
Si l'id n'existe pas

Notes: 
Pas de gestion d'erreur pour le moment

Documentation Rédigé par Adzuar Rémy the Serendipity Magician
le 2020-06-04
