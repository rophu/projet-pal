Insert into user values (1, "Association","association@gmail.com","Orléans","Super association qui promeut le jeu en société", "absolu");
Insert into user values (2, "gégé", "jacky@gmail.com","Sandillon","Joueur invétéré!","aucun");
Insert into user values (3, "bob", "bmarley@wailers.org","Saran","Tu pourra pas me battre !","moderator");
Insert into user values (4, "FalconX", "anar@topsecret.com", "Orléans","pas de MP les RG veillent!","aucun");
Insert into user values (5, "Ana", "ana@gmail.com","Orléans","Toujours prête à te faire mordre la poussière", "moderator");

Insert into jeu values (1,"Labyrinthe", "Nathan", "Folio SF", 1936, 120, "logique", 2, 2, 4, "Un jeu facile à programmer, mais plus sympa à jouer en vrai");
Insert into jeu values (2,"Solitaire", "Ravensburger", "Folio SF", 1789, 15, "hasard", 1, 1, 1, "Un jeu de société pour les associaux");
Insert into jeu values (3,"Bataille navale", "Nathan", "Gallimard", 1968, 5, "tactique", 1, 2, 2, "Touché! coulé!");
Insert into jeu values (4,"Risk", "Nathan", "Gallimard", 1993, 120, "strategie", 3, 2, 5, "Un jeu pour régler ses conflits");

Insert into posseder values (1, 1, 2, 0, 0, 5, "aucune", 2010, "comme neuf");
Insert into posseder values (2, 2, 3, 5, 1, 1, "aucune", 2002, "bon état");
Insert into posseder values (3, 3, 4, 2, 1, 1, "aucune", 2005, "correct");
Insert into posseder values (4, 5, 1, 3, 1, 2, "aucune", 2000, "correct");


Insert into `group` values (1, "Dream team", "La meilleure des équipes", true, DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci`);

Insert into inclure values (1,2,1,true);
Insert into inclure values (1,3,1,false);















































--	
--                  __--___
--                 >_'--'__'
--                _________!__________
--               /   /   /   /   /   /
--              /   /   /   /   /   /
--             |   |   |   |   |   |
--        __^  |   |   |   |   |   |
--      _/@  \  \   \   \   \   \   \
--     S__   |   \   \   \   \   \   \         __
--    (   |  |    \___\___\___\___\___\       /  \
--        |   \             |                |  |\|
--        \    \____________!________________/  /
--         \ _______OOOOOOOOOOOOOOOOOOO________/
--          \________\\\\\\\\\\\\\\\\\\_______/
--%%%^^^^^%%%%%^^^^!!^%%^^^^%%%%%!!!!^^^^^^!%^^^%%%%!!^^
--^^!!!!%%%%^^^^!!^^%%%%%^^!!!^^%%%%%!!!%%%%^^^!!^^%%%!!

